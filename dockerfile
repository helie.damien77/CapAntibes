# Étape de construction
FROM node:16-alpine AS builder

# Définit le répertoire de travail
WORKDIR /app

# Copie package.json et package-lock.json
COPY package*.json ./

# Installe les dépendances
RUN npm install

# Copie le reste des fichiers de l'application
COPY . .

# Construit l'application
RUN npm run build

# Étape de production
FROM node:16-alpine

# Définit le répertoire de travail
WORKDIR /app

# Copie package.json et package-lock.json
COPY package*.json ./

# Installe les dépendances de production seulement
RUN npm install --only=production

# Copie les fichiers construits depuis l'étape de construction
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/public ./public

# Expose le port sur lequel l'application sera accessible
EXPOSE 3000

# Lance l'application
CMD ["npm", "start"]
