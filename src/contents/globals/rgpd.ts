import { IContentByLang } from '@/interfaces'

export const contentsRgpd: Record<string, IContentByLang> = {
  text: {
    fr: `En poursuivant votre navigation sur ce site, vous acceptez notre politique de protection des données personnelles et l'utilisation de cookies`,
    en: `By continuing to browse this site, you accept our privacy policy and the use of cookies.`,
    ru: `En poursuivant votre navigation sur ce site, vous acceptez notre politique de protection des données personnelles et l'utilisation de cookies`
  },
  button: {
    fr: `J'ai compris`,
    en: 'I agree',
    ru: `ru-J'ai compris`
  }
}
