import { IContentByLang } from '@/interfaces'

export const contentsToBook: Record<string, IContentByLang> = {
  title: {
    fr: 'Réserver une chambre',
    en: 'To book a room',
    ru: 'ru-To book a room'
  },
  title_mobile: {
    fr: 'Une chambre',
    en: 'A room',
    ru: 'ru-A room'
  },
  period: {
    fr: 'Dates',
    en: 'Dates',
    ru: 'ru-Dates'
  },
  period_arrival: {
    fr: 'Arrivée',
    en: 'Arrival',
    ru: 'ru-Arrival'
  },
  period_departure: {
    fr: 'Depart',
    en: 'Departure',
    ru: 'ru-Departure'
  },
  adults: {
    fr: 'Adultes',
    en: 'Adults',
    ru: 'ru-Adults'
  },
  children: {
    fr: 'Enfants',
    en: 'Children',
    ru: 'ru-Children'
  },
  code: {
    fr: 'Code',
    en: 'Code',
    ru: 'ru-Code'
  },
  button: {
    fr: 'Voir les tarifs',
    en: 'See the prices',
    ru: 'ru-See the prices'
  }
}
