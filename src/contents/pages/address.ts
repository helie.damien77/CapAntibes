import { IContentByLang } from '@/interfaces'

export const contentsAddress: Record<string, IContentByLang> = {
  introduction: {
    fr: `Adresse géographique de l’hôtel.`,
    en: `Geographical address of the hotel`,
    ru: `ru-La localisation du lHOTEL na nordiuropon Etris. Locciae invescia vero ve, loremp quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int. An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moc. oc.`
  },
  subTitle1: {
    fr: `L’adresse`,
    en: `The Address`,
    ru: `localisation`
  },
  text1: {
    fr: `L’hôtel, avec ses lignes pures et fortes, s’intègre au décor fascinant  d’Antibes. La route longeant la presqu’île est l’une des plus belle de la French Riviera. Vous vous baignez dans de petites criques où seuls quelques petits bateaux pointus sont à l’ancre. Entre Nice, Toulon et Monaco, le Beach Hotel se situe sur la péninsule sauvage du Cap d’Antibes. Il est abrité entre les ports Gallice et du Crouton. Tourné vers la mer, le soleil se couche sur la Méditerranée, avec à l’horizon les îles de Lérins et Golf Juan.`,
    en: `The hotel, with its pure and strong lines, blends into the fascinating landscape of Antibes. The road along the peninsula is one of the most beautiful on the French Riviera. You can swim in small coves where only a few small pointed boats are anchored. Between Nice, Toulon, and Monaco, the Beach Hotel is located on the wild peninsula of Cap d'Antibes. It is sheltered between the Gallice and Crouton ports. Facing the sea, the sun sets over the Mediterranean, with the Lérins Islands and Golf Juan on the horizon.`,
    ru: `La localisation du luxe na nordiuropon Etris. Locciae invescia vero ve, loremp quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int. An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moc. Locciae invescia vero ve, loremp quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int. An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moc.`
  },
  subTitle2: {
    fr: `comment accéder à l’hôtel`,
    en: `How to get to the hotel`,
    ru: `comment accéder à l’hôtel`
  },
  text2: {
    fr: `• En avion : à 15km de l’aéroport International Nice Côte d’Azur et de l’Aérodrome de Cannes Mandelieu pour les vols en jets privés`,
    en: `• By plane: 15km from Nice Côte d'Azur International Airport and Cannes Mandelieu Aerodrome for private jet flights.`,
    ru: ``
  },
  text3: {
    fr: `• En voiture : Depuis l’autoroute A8, prendre la sortie n° 44 Antibes direction Juan les Pins. Au départ de l’aéroport de Nice, prenez le bord de mer en longeant la côte.`,
    en: `• By car: From the A8 motorway, take exit 44 Antibes towards Juan les Pins. From Nice airport, take the seafront along the coast.`,
    ru: ``
  },
  text4: {
    fr: `• En bateau :  Depuis la mer, en accostant au Port Gallice qui jouxte notre hôtel. Vous pourrez prendre contact avec la capitainerie du Port qui vous autorisera à accoster avant que votre capitaine ne retourne au mouillage dans la baie non loin. Coordonnées GPS : 43,561618 – 7,118211. `,
    en: `• By boat: From the sea, dock at Port Gallice, next to our hotel. You can contact the harbour master's office, who will authorise you to berth before your captain returns to the anchorage in the nearby bay. GPS coordinates: 43.561618 - 7.118211.`,
    ru: ``
  },
  text5: {
    fr: `• En train : Depuis la Gare TGV d’Antibes ville. Vous pouvez également descendre à Cannes et prendre la correspondance pour Juan-Les Pins, une gare intermédiaire qui se trouve à 1,4 km.`,
    en: `• By train: From Antibes TGV station. You can also get off at Cannes and take the connecting train to Juan-Les Pins, an intermediate station 1.4 km away.`,
    ru: ``
  }
}
