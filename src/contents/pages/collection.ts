import { IContentByLang } from '@/interfaces'

export const contentsCollection: Record<string, IContentByLang> = {
  introduction: {
    fr: `Écrire et faire vivre des histoires, redorer de Belles Endormies, imaginer des atmosphères uniques pour accueillir ses voyageurs : c’est la raison d’être du groupe Chapitre Six. Raconter des adresses iconiques, partager la modernité d’ambiances authentiques, célébrer chaque passage, de jour et de nuit, (ré)inventer des maisons… 
    À chaque lieu son histoire, son expérience, entre passé et futur, rêve et réalité.
    À Paris bien sûr, à Montmartre, dans le Marais, sur les Grands Boulevards, à Saint-Germain, près de l’Opera Garnier ou les Champs Élysées. À Saint-Tropez et au Cap d’Antibes, très prochainement dans la forêt de Barbizon ou bientôt à la montagne… 
    Les nuits sont douces, les tables joyeuses, on s’y régale autant des jeux coloriels des décors que des mariages de saveurs imaginés par des esthètes du goût. Comme à la maison, on regarde, on vit, on revient.`,
    en: `To write and bring to life stories, to restore Belles Endormies, to imagine unique atmospheres to welcome travelers: this is the raison d'être of the Chapitre Six group. Telling the story of iconic addresses, sharing the modernity of authentic ambiences, celebrating every passage, day and night, (re)inventing homes... 
    Each place has its own story, its own experience, between past and future, dream and reality.
    In Paris, of course, in Montmartre, the Marais, the Grands Boulevards, Saint-Germain, near the Opera Garnier or the Champs Élysées. In Saint-Tropez and Cap d'Antibes, very soon in the Barbizon forest or soon in the mountains... 
    The nights are balmy, the tables are cheerful, and you'll enjoy the colorful décor as much as the marriage of flavors imagined by aesthetes of taste. It's like being at home, watching, experiencing and returning.
`,
    ru: `Une succession de coups de cœur guidée par l’envie de créer des lieux uniques à l’image de leur destination. On dîne sous la belle étoile de la Butte Montmartre chez Monsieur Aristide, on s’installe dans une suite mystérieuse, cachée dans le jardin de Monsieur George. A Saint-Tropez, on s’encanaille au bord de l’eau sur un air de jazz à l’hôtel La Ponche. Ils font vivre aux voyageurs l’esprit, l’âme d’une ville ou d’un quartier. Ils sont une rencontre. Car nous sommes convaincus que l’hôtel dans lequel on se rend fait désormais partie du voyage. Qu’il doit raconter aussi une histoire. Les hôtels Adresses ne ressemblent à aucun autre. Ils sont éclectiques, festifs et élégants.`
  },
  button: {
    fr: `Découvrir`,
    en: `Discover`,
    ru: `Découvrir`
  }
}
