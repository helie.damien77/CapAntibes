import { IContentByLang } from '@/interfaces'

export const contentsHome: Record<string, IContentByLang> = {
  title1: {
    fr: `bienvenue au cap d’antibes`,
    en: `Welcome to Cap d'Antibes Beach Hotel`,
    ru: `Добро пожаловать в Cap D’Antibes Beach Hotel.`
  },
  text1_1: {
    fr: `Le Soleil. La Mer. Le Sable...`,
    en: `The Sun. The Sea. The Sand...`,
    ru: `Солнце.Море.Белый песок...`
  },
  text1_2: {
    fr: `Le Beach Hotel, 35 chambres et 5 étoiles. Les pieds dans l’eau entre Cannes et Nice. Avec sa plage privée, c’est une adresse dédiée à la joie de vivre. Le matin, seul au monde, vous prenez votre premier bain de mer. Au fil des vagues, la musique monte avec le soleil. La fête embrase BABA, le restaurant de plage. Le bar en rotonde est un balcon sur la Méditerranée. Au calme, à l’ombre dans le jardin exotique ou sur la pergola du bassin à débordement, rejoignez l’espace bien-être. Vous laissez filer vos pensées avec le vent. Le restaurant étoilé Les Pêcheurs dresse ses nappes blanches pour le dîner. Vous vous endormez avec le bruit des vagues et une certitude. Demain il fera beau.`,
    en: `The Beach Hotel, a 5 stars with 35 rooms and suites, is located right on the water between Cannes and Nice. With its private beach, it is an address dedicated to the joy of living. In the morning, you can take your first swim in the sea all by yourself. As the waves roll in, the music rises with the sun. The party ignites at BABA, the beach restaurant. The circular bar is a balcony overlooking the Mediterranean. In peace and quiet, in the shade of the exotic garden or on the pergola of the infinity pool, you can join the wellness area. You let your thoughts drift away with the wind. The Michelin starred Les Pêcheurs sets its white tablecloths for dinner. You fall asleep to the sound of the waves with the certainty that tomorrow will be a beautiful day.`,
    ru: `Beach Отель,35 комнат и 5 звезд.Райский уголок между Каннами и Ниццей. Его частный пляж подарит Вам радость жизни. Каждое утро,окунаясь в море, почувсвуйте себя наедине с миром. Когда волны накатывают, музыка поднимается вместе с солнцем. Праздник начинается в « BABA »,нашем ресторане у пляжа,чей бар открывает чудесный вид на Средиземное море. В полной тишине, в тени экзотического сада или на беседке у бассейна, присоединяйтесь к нашим спа-процедурам. Позвольте себе отстраниться от оружающего мира и всех его тревог. В это время гастрономический ресторан « Les Pêcheurs » накроет свои белые скатерти для изысканного ужина, после которого ночью вы заснете с полной уверенностью в завтрашнем дне.`
  },
  title2: {
    fr: `les chambres et suites`,
    en: `Rooms and Suites`,
    ru: `Номера и люксы`
  },
  text2: {
    fr: `Avec ses 35 chambres et suites à partir de 30m2, vous êtes au Beach Hotel comme dans une maison. Vous accédez aux chambres par les coursives en lames de bois laquées blanches. Le sol en pierre s’étend de l’extérieur vers l’intérieur. Certaines disposent d’un jardin privé et d’autres, à l’étage, ont des vues imprenables depuis leur terrasse. Dans les vastes salles de bains, la lumière naturelle se mêle aux marbres roses. Des banquettes forment un salon intérieur, on peut lire un livre, rêver devant la vue ou dîner dans sa chambre sur la table basse en bois.`,
    en: `With 35 rooms and suites, from 30m², you will feel at home at the Beach Hotel. You enter the rooms through white lacquered wooden walkways. The stone flooring extends from the outdoor spaces to the inside of the rooms. Some rooms have a private garden, while others on the upper floors have breathtaking views from their terrace. In the spacious bathrooms, natural light blends with pink marble. Sofas form an indoor lounge where you can read a book, daydream while enjoying the view, or even dine in your room on the wooden coffee table. `,
    ru: `В 35 номерах и люксах площадью не менее 36 м2, Вы почувствуете себя как дома. Номера,продуманные до мелочей,позволят Вам оказаться в безмятежности.Каменная плитка всретит Вас у входа,а деревянные трапы, покрытые белым лаком,проведут Вас до комнат. В некоторых номерах есть частный сад, а из других, расположенных на верхних этажах, открывается вид,захватывающий дух. Скамьи,расположенные в номере, образуют крытую гостиную, где можно почитать книгу, помечтать, любуясь видом, или пообедать в собственном номере на деревянном журнальном столике. Вы можете поставить свои чемоданы в большой шкаф из красного дерева. В огромных ванных комнатах естественный свет сочетается с розовым мрамором.`
  },
  title3_1: {
    fr: `les pêcheurs,`,
    en: `les pêcheurs,`,
    ru: `les pêcheurs,`
  },
  title3_2: {
    fr: `restaurant étoilé`,
    en: `Michelin starred restaurant`,
    ru: `Гастрономический ресторан`
  },
  subTitle3: {
    fr: `de 19h30 à 22h00`,
    en: `from 7:30 pm to 10:00 pm`,
    ru: `de 7h30 à 10h30`
  },
  text3: {
    fr: `Le restaurant Les Pêcheurs est une institution depuis les années 1940. Chaque matin, au retour du bateau de pêche, Le chef Nicolas Rondelli propose une carte renouvelée selon la générosité de la mer. Tous les soirs, vous embarquez aux Pêcheurs comme à bord d’un yacht du début du siècle. Le sol en marbre rouille s’avance au-dessus de la Méditerranée. Les murs en acajou vernis se jouent des reflets de l’eau. Les grandes baies vitrées s’effacent comme à l’arrière d’un bateau dominant la mer.`,
    en: `The restaurant Les Pêcheurs has been an institution since the 1940s. Every morning, upon the return of the fishing boat, Chef Nicolas Rondelli offers a menu renewed according to the generosity of the sea. Every evening, you embark at Les Pêcheurs as if on a turn-of-the-century yacht. The rust-colored marble floor extends over the Mediterranean. The polished mahogany walls play with the reflections of the water. The large windows disappear like at the back of a boat overlooking the sea.`,
    ru: `Ресторан « Les Pêcheurs » - это заведение 1940-х годов. Каждое утро,по возвращении рыболовецкого судна, шеф-повар Николя Ронделли предлагает обновленное меню в соответствии с щедростью моря. Каждый вечер вы отправляетесь в « Les Pêcheurs », как будто на борту яхты, построенной в конце прошлого века. Мраморный пол цвета палубы возвышается над Средиземноморьем. Лакированные стены из красного дерева играют с отражениями воды.Большие эркеры исчезают, словно задние борты судна с видом на море.`
  },
  title4_1: {
    fr: `BABA`,
    en: `BABA`,
    ru: `BABA`
  },
  title4_2: {
    fr: `Restaurant Bar Plage`,
    en: `Beach Restaurant Bar`,
    ru: `Ресторан бар на пляже`
  },
  subTitle4: {
    fr: `de 12h00 à 23h30`,
    en: `from 12:00 am to 11:30 pm`,
    ru: `de 7h30 à 10h30`
  },
  text4_1: {
    fr: `BABA, le restaurant de l’hôtel à ciel ouvert, est orchestré en partenariat avec le chef Assaf Granit et ses équipes. Toute la journée, sa cuisine méditerranéenne voyage depuis Jérusalem. Les magnifiques parfums d’épices s’échappent de la cuisine en plein air. Autour du bar en rotonde, l’ambiance est à l’insouciance. La musique et l’énergie du Cap Jaffa montent dans le ciel.`,
    en: `BABA, the hotel's open-air restaurant, is orchestrated in partnership with Chef Assaf Granit and his team. Throughout the day, their Mediterranean cuisine travels from Jerusalem. The magnificent spice aromas escape from the outdoor kitchen. Around the circular bar, the atmosphere is carefree. The music and energy of Cap Jaffa rise into the sky.`,
    ru: `« BABA », ресторан под открытым небом, приветсвует Вас с шеф-поваром Ассаф Гранит и его командой. В течение всего дня его средиземноморская кухня шлет Вам привет из Иерусалима. Прекрасные ароматы специй прорываются из кухни на открытый воздух. Вокруг бара-ротонды царит беззаботная атмосфера. Музыка и энергия мыса Яффо поднимается в небо.`
  },
  text4_2: {
    fr: `C’est une fête, un grand soleil joyeux qui relie les rives de la Méditerranée.`,
    en: `It's a celebration, a big joyful sun that connects the shores of the Mediterranean Sea.`,
    ru: `Это настоящий праздник, великое радостное солнце, соединяющее берега Средиземного моря.`
  },
  title5: {
    fr: `FARNIENTE`,
    en: `FARNIENTE`,
    ru: `Полный релакс`
  },
  text5: {
    fr: `Rareté absolue sur la Côte d’Azur, le Beach Hôtel dispose de sa propre plage. On profite d’une douce oisiveté sur un bain de soleil. On boit des cocktails entre amis. Autour du bar, on partage une partie de backgammon en famille. Vous partez en promenade en pédalo, à vélo ou en bateau. Vous pouvez vous détendre à l’abri dans le jardin exotique, à l’ombre d’un palmier ou autour de la piscine.`,
    en: `An absolute rarity on the French Riviera, the Beach Hotel has its own beach. Guests can enjoy a sweet idleness on a sun lounger and sip cocktails with friends. Around the bar, families can share a game of backgammon. Guests can go for a walk on a pedalo, by bike, or by boat. They can relax in the exotic garden, in the shade of a palm tree, or around the pool.`,
    ru: `Абсолютная редкость на Лазурном берегу – Beach отель имеет собственный пляж.Наслаждайтесь нежным бездельем, принимая солнечные ванны.Пейте коктейли с друзьями. Около бара играйте в нарды с семьей. Отправляйтесь покататься на водном велосипеде или лодке.Отдохните в укрытии экзотического сада, в тени пальмы или у бассейна.`
  },
  title6: {
    fr: `BIEN-ÊTRE`,
    en: `Wellness`,
    ru: `Спа`
  },
  text6: {
    fr: `L’espace bien-être prolonge la douceur de vivre des vacances au bord de l’eau. Nous employons des produits holistiques de la gamme Holidermie, naturels et organiques. Pour votre bien-être intérieur, le Tigre dispense des cours de yoga en salle ou en plein air : venez saluer le soleil qui se lève face à la mer. Un espace à ciel ouvert, abrité par des voilages, se trouve dans notre jardin à l’ombre du Jacaranda.`,
    en: `The wellness area extends the gentle living of holidays by the water. Holistic products from the Holidermie range, which are natural and organic, are used. For inner well-being, Le Tigre offers yoga classes in the studio or outdoors: come and greet the rising sun facing the sea. An open-air space, sheltered by voiles, is located in our garden under the Jacaranda tree.`,
    ru: `Велнес-зона продлевает удовольствие от отдыха у воды.Мы используем холистические продукты из серии Homemie, натуральные и органические. Для вашего внутреннего благополучия « Le Tigre » предлагает занятия йогой в помещении и на открытом воздухе: приходите и встречайте восход солнца на берегу.Площадка под открытым небом, защищенная тенью джакаранды ждет Вас каждое утро.`
  },
  btnMoreInformation: {
    fr: 'découvrir',
    en: 'More information',
    ru: 'откройте для себя'
  },
  title7: {
    fr: `Un air de californie`,
    en: `AN AIR OF CALIFORNIA`,
    ru: `ru-Un air de californi`
  },
  text7: {
    fr: `
Le Beach Hotel a été repensé par l’architecte Bernard Dubois comme un hommage aux maisons géométriques et épurées de Palm Springs ainsi qu’aux villas méditerranéennes. Rêvé avec le soleil et la mer, l’hôtel joue des ombres et des lumières sur les murs en crépis blanc. C’est une structure géométrique, brute et charmante. Un manifeste face à la mer. Deux forces face à face. Décor mêlant végétal et minéral. Les perspectives s’ouvrent sur le cadre naturel du cap d’Antibes qui a souvent inspiré le cinéma. 
Entre les ports Gallice et du Crouton, le Beach Hotel est l’une des adresses iconiques de la Côte d’Azur.`,
    en: `The Beach Hotel has been redesigned by architect Bernard Dubois as a tribute to the geometric, streamlined houses of Palm Springs and Mediterranean villas. Dreamt up with the sun and the sea, the hotel plays with light and shadow on the white rendered walls. It is a geometric structure, rough and charming. A manifesto facing the sea. Two forces face each other. A décor combining plant and mineral elements. The views open out onto the natural setting of Cap d'Antibes, which has often inspired the cinema. Between the ports of Gallice and Crouton, the Beach Hôtel is one of the Côte d'Azur's iconic addresses.`,
    ru: `Велнес-зона продлевает удовольствие от отдыха у воды.Мы используем холистические продукты из серии Homemie, натуральные и органические. Для вашего внутреннего благополучия « Le Tigre » предлагает занятия йогой в помещении и на открытом воздухе: приходите и встречайте восход солнца на берегу.Площадка под открытым небом, защищенная тенью джакаранды ждет Вас каждое утро.`
  }
}
