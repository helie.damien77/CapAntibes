import { IContentByLang } from '@/interfaces'

export const contentsFarniente: Record<string, IContentByLang> = {
  title: {
    fr: 'Farniente',
    en: 'Farniente',
    ru: 'ru-Farniente'
  },
  description: {
    fr: 'Le Beach Hôtel dispose d’un accès exclusif à la plage privée des Pêcheurs. Oisiveté sur un bain de soleil, cocktails entre amis ou promenade en pédalo. Le soleil brille, le temps suspend son vol.',
    en: 'The Beach Hôtel has exclusive access to the private Pêcheurs beach. Idle sunbathing, cocktails with friends or a pedalo ride. The sun shines and time stands still.',
    ru: 'ru-la plage sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  subTitle1: {
    fr: 'La plage',
    en: 'The beach',
    ru: 'ru-La plage'
  },
  text1: {
    fr: 'A la lisière d’une plage de sable blanc et rose sous le soleil de la Côte d’Azur. La mer est un lac bleu transparent. Vous traversez la pelouse du jardin et plongez dans la mer face aux îles de Lérins. Protégeant les bains de soleil, les parasols roses se balancent légèrement au gré du vent sur la seule plage privée de la Côte.',
    en: 'At the edge of a white and pink sand beach under the sun of the Côte d’Azur. The sea is a transparent blue lake. You cross the lawn of the garden and dive into the sea facing the Lérins islands. Protecting the sun loungers, the pink parasols sway gently in the wind on the only private beach on the coast.',
    ru: 'ru-la plage sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  subTitle2: {
    fr: 'Bassin sensoriel',
    en: 'Sensory Pool',
    ru: 'ru-Bassin sensorielle'
  },
  text2: {
    fr: 'Dominant la Méditerranée, le bassin sensoriel semble se prolonger sans fin dans la mer. Il est bordé d’une grande terrasse en étages. En surplomb, au calme d’une pergola, la vue est imprenable. Le bassin fait le bonheur de ceux qui préfèrent l’eau douce et chaude, notamment si le mistral se lève. ',
    en: 'Overlooking the Mediterranean, the sensory pool seems to extend endlessly into the sea. It is bordered by a large multi-level terrace. Overlooking it, in the quiet of a pergola, the view is breathtaking. The pool delights those who prefer fresh and warm water, especially if the mistral wind picks up.',
    ru: 'ru-le bien être sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero.'
  },
  subTitle3: {
    fr: 'La Boutique de Plage',
    en: 'The Beach Boutique',
    ru: 'ru-Jardin botanique'
  },
  text3: {
    fr: 'Emportez un peu de la douceur du Cap d’Antibes Beach Hotel. Nichée en bord de mer, la boutique est une escale pour sublimer vos journées ensoleillées. Essentiels de plage ou produits aux couleurs de l’hôtel, à garder pour soi ou à offrir… tout ici vous rappellera les moments passés sous le soleil de la Côte d’Azur.',
    en: 'Take a little of the softness of the Cap d’Antibes Beach Hotel with you. Nestling on the seafront, the boutique is the perfect place to spend your sunny days. Beach essentials or products in the hotel colours, to keep for yourself or to give as gifts... everything here will remind you of moments spent under the Côte d’Azur sun.',
    ru: 'ru-le jardin botanique sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  subTitle4: {
    fr: 'Jardin exotique',
    en: 'Exotic Garden',
    ru: 'ru-Jardin botanique'
  },
  text4: {
    fr: 'Le jardin exotique longe l’hôtel jusqu’à la plage. On parcourt le monde en s’y promenant. Il s’inscrit dans la tradition des jardins d’acclimatation de la Côte d’Azur. Les premières essences d’arbres, rapportées des voyages aux long cours, ont été plantées sur la péninsule d’Antibes. Laissez-vous charmer par les compositions graphiques des plantes rares et la variété de leurs couleurs. L’incroyable floraison du Jacaranda magnétise le jardin. Il laisse au sol un tapis de fleurs violettes. L’été, les fleurs des arbres à goupillons australiens offrent un feu d’artifices de couleurs.',
    en: 'The exotic garden runs alongside the hotel right down to the beach. You can travel the world walking through it. It follows in the tradition of the acclimatisation gardens on the Côte d’Azur The first tree species, brought back from ocean voyages, were planted on the Antibes peninsula. Let yourself be charmed by the graphic compositions of the rare plants and the variety of their colours. The incredible flowering of the Jacaranda magnetises the garden. It leaves a carpet of purple flowers on the ground. In summer, the flowers of the Australian pinyon trees offer a firework display of colour.',
    ru: 'ru-le jardin botanique sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  btn4: {
    fr: 'Découvrir',
    en: 'Discover',
    ru: 'ru-Découvrir'
  },
  subTitle5: {
    fr: `Kids Club`,
    en: `Kids Club`,
    ru: `ru-Kids Club`
  },
  text5: {
    fr: 'Pour que parents et enfants profitent tout autant de leur séjour ou de leur déjeuner, le Kids club propose des activités toute la journée. Ateliers artistiques, olympiades sportives, chasses au trésor, déjeuner et goûter dans le jardin exotique. Pour nos aventuriers du monde entier, les animateurs parlent anglais et français.',
    en: 'To ensure that parents and children enjoy their stay or lunch equally, the Kids Club offers activities all day long. Art workshops, sports Olympics, treasure hunts, lunch, and snack in the exotic garden. For our young adventurers from all over the world, the staff speaks English and French.',
    ru: 'ru-la location nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit'
  },
  btn5: {
    fr: 'Découvrir',
    en: 'Discover',
    ru: 'ru-Découvrir'
  },
  subTitle6: {
    fr: 'Les Escapades',
    en: 'Escapades',
    ru: 'ru-Vélo et course à pied'
  },
  text6: {
    fr: 'Par les terres ou par la mer, le Cap d’Antibes est l’une des plus belles côtes de France à découvrir. Les falaises de calcaire blanc du littoral plongent dans la mer et offrent un décor idéal pour les randonnées. Empruntez le Sentier des douaniers, parsemé de pins maritimes bercés par le vent et les embruns. A vélo, les balades sont toujours synonymes d’évasion. Des bicyclettes électriques sont mises à disposition par le Beach Hôtel. Ou prenez le large en empruntant les pédalos de l’hôtel. Vous pouvez également louer des paddles, scooters de mer et bateaux à moteur sur le port avoisinant de la Gallice.',
    en: 'Whether by land or sea, the Cap d’Antibes is one of France’s most beautiful coastlines to discover. The white limestone cliffs along the coast plunge into the sea, providing the perfect backdrop for walks. Take the Sentier des douaniers, a path dotted with maritime pines cradled by the wind and sea spray. Cycling is always a great way to get away from it all. Electric bicycles are available from the Beach Hôtel. Or take to the open sea on one of the hotel’Antibes pedalos. You can also hire paddles, sea scooters and motorboats from the nearby port of La Gallice.',
    ru: 'ru-la location nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit'
  },
  btn6: {
    fr: 'Découvrir',
    en: 'Discover',
    ru: 'ru-Découvrir'
  }
}
