import { IContentByLang } from '@/interfaces'

export const contentsPrivatisation: Record<string, IContentByLang> = {
  title: {
    fr: 'ÉVÈNEMENTIEL',
    en: 'EVENTS',
    ru: 'ru-PRIVATISATION'
  },
  description: {
    fr: 'Parce que le Cap d’Antibes Beach Hôtel est un lieu aussi unique qu’inattendu, il est l’écrin idéal pour accueillir un évènement que vous souhaitez inoubliable. Lancement officiel, mariage, anniversaire … Nous créons des formules sur-mesure selon vos besoins et envies, en privatisant au choix nos espaces et restaurants : Les Pêcheurs, BABA, la piscine, la plage, ou l’intégralité de l’établissement. Avec ses 35 chambres et suites 5 étoiles, l’hôtel vous invite également à prolonger ces instants d’exception. Après une journée festive au bord de l’eau, vos invités se retirent dans le confort de nos chambres, bercés par le doux bruit des vagues. Et au réveil, un petit-déjeuner face à la Méditerranée les attend pour clôturer leur séjour sur une note mémorable.',
    en: 'Because the Cap d’Antibes Beach Hôtel is as unique as it is unexpected, it is the ideal setting to host an event that you hope will be unforgettable. Official launch, wedding, birthday ... We create tailor-made formulas according to your needs and desires, by privatizing our choice of spaces and restaurants: Les Pêcheurs, BABA, the swimming pool, the beach, or the entire establishment. With its 35 5-star rooms and suites, the hotel also invites you to extend these exceptional moments. After a festive day by the water, your guests can retire to the comfort of our rooms, lulled by the gentle sound of the waves. And when they wake up, a breakfast overlooking the Mediterranean awaits to end their stay on a memorable note.',
    ru: 'ru-la plage sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  discover: {
    fr: `Découvrir`,
    en: `Discover`,
    ru: `Découvrir`
  },
  button: {
    fr: 'Nous contacter',
    en: 'Contact us',
    ru: 'ru-Réserver'
  },
  subTitle1: {
    fr: 'Célébration',
    en: 'Celebration',
    ru: 'ru-les séminaires'
  },
  text1: {
    fr: 'Le temps d’une heure ou d’une journée, le cadre enchanteur du Cap d’Antibes peut accueillir votre célébration familiale, festive ou intime, de jour ou de nuit. A vos couleurs et selon vos attentes, l’hôtel sera la scène d’un moment de vie inoubliable.',
    en: 'For an hour or a day, the enchanting setting of Cap d’Antibes can host your family celebration, festive or intimate, day or night. In your colours and according to your expectations, the hotel will be the stage for an unforgettable moment in your life.',
    ru: 'ru-la plage sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  subTitle2: {
    fr: 'les séminaires',
    en: 'Seminars',
    ru: 'ru-les séminaires'
  },
  text2: {
    fr: 'Pour que le temps de travail, de partage et d’échange soit aussi joyeux que productif, le Cap d’Antibes Beach hôtel accueille votre séminaire d’entreprise avec des formules originales, gourmandes et studieuses, festives et gastronomiques. Un moment particulier pour enchanter, motiver et booster la créativité de vos équipes.',
    en: 'To ensure that the time spent working, sharing and exchanging is as joyful as it is productive, the Cap d’Antibes Beach hotel hosts your company seminar with original, gourmet and studious, festive and gastronomic formulas. A special moment to enchant, motivate and boost the creativity of your teams.',
    ru: 'ru-le bien être sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero.'
  },
  subTitle3: {
    fr: 'Les privatisations',
    en: 'Privatisations',
    ru: 'ru-célébration'
  },
  text3: {
    fr: 'Pour hisser haut vos couleurs sur la merveilleuse plage du Cap d’Antibes Beach hôtel, privatisez entièrement l’hôtel ! L’occasion d’offrir à vos invités une immersion dans un décor unique et personnalisé selon vos envies et votre cahier des charges. Toute l’équipe sera à votre disposition pour faire de l’hôtel VOTRE maison.',
    en: 'To raise your colours high on the marvellous Cap d’Antibes Beach hotel, privatise the entire hotel! This is your chance to immerse your guests in a unique setting, personalised to your wishes and specifications. The entire team will be on hand to make the hotel YOUR home.',
    ru: 'ru-le jardin botanique sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  },
  subTitle4: {
    fr: 'La salle de réunion',
    en: 'The meeting room',
    ru: 'ru-célébration'
  },
  text4: {
    fr: 'Modulable à souhait, notre salle de réunion peut accueillir jusqu’à 35 personnes en format assis. Elle dispose d’un grand écran pour organiser présentations ou visioconférences, et est située à proximité des restaurants pour prolonger les réunions autour d’une table conviviale.',
    en: 'Our flexible meeting room can accommodate up to 35 people seated. It has a large screen for presentations or videoconferences, and is located close to the restaurants so you can extend your meetings over a convivial meal.',
    ru: 'ru-le jardin botanique sIgna nordiuropon Etris. Decteri ssendem audem iusquod condem ad mo turebatiam vid consu inatodi ctorarit. Locciae invescia vero ve, quidendi pra, essenatabem consunum effreviverum ex se ante factatque miliniq uostrum int? An vermis, fursul vid sedite confer audende ntiaede mquit. Untrenihicae moC. Muliam ium urnum poenduc idiis. Rorsupi cavero in tabit.'
  }
}
