import { IContentByLang } from '@/interfaces'

export const contentsMention: Record<string, IContentByLang> = {
  title: {
    fr: `Propriétaire du site`,
    en: `Website owner`,
    ru: `Propriétaire du site`
  },

  ligne1: {
    fr: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON» `,
    en: `SAS «LES PECHEURS PORT DU CROUTON» `,
    ru: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON» `
  },
  ligne2: {
    fr: `10, Bd Maréchal Juin, `,
    en: `10, Bd Maréchal Juin, `,
    ru: `10, Bd Maréchal Juin, `
  },
  ligne3: {
    fr: `06160 Antibes, France `,
    en: `06160 Antibes, France `,
    ru: `06160 Antibes, France `
  },
  ligne4: {
    fr: `Email : contact@cabh.fr `,
    en: `Email : contact@cabh.fr `,
    ru: `Email : contact@cabh.fr `
  },
  ligne5: {
    fr: `www.capdantibes-beachhotel.com`,
    en: `www.capdantibes-beachhotel.com`,
    ru: `www.capdantibes-beachhotel.com`
  },

  ligne6: {
    fr: `INFORMATIONS LEGALES`,
    en: `INFORMATIONS LEGALES`,
    ru: `INFORMATIONS LEGALES`
  },
  ligne7: {
    fr: `Nom Commercial : Cap d’Antibes Beach Hotel`,
    en: `Trade name : Cap d’Antibes Beach Hotel`,
    ru: `Nom Commercial : Cap d’Antibes Beach Hotel`
  },
  ligne8: {
    fr: `Forme Juridique : SAS`,
    en: `Legal Form : SAS`,
    ru: `Forme Juridique : SAS`
  },
  ligne9: {
    fr: `Capital : 2 000 000,00 €`,
    en: `Capital : 2 000 000,00 €`,
    ru: `Capital : 2 000 000,00 €`
  },
  ligne10: {
    fr: `SIRET : 44358313300022`,
    en: `SIRET : 44358313300022`,
    ru: `SIRET : 44358313300022`
  },
  ligne11: {
    fr: `RCS : Antibes B 351 863 089 `,
    en: `RCS : Antibes B 351 863 089 `,
    ru: `RCS : Antibes B 351 863 089 `
  },
  ligne12: {
    fr: `Code APE : 5510Z`,
    en: `Code APE : 5510Z`,
    ru: `Code APE : 5510Z`
  },
  ligne13: {
    fr: `N° TVA INTRACOM : FR68351863089`,
    en: `VAT INTRACOM N° : FR68351863089`,
    ru: `N° TVA INTRACOM : FR68351863089`
  },

  title2: {
    fr: `Hébergeur`,
    en: `Host `,
    ru: `Hébergeur `
  },
  subtitle2: {
    fr: `OVH`,
    en: `OVH`,
    ru: `OVH`
  },
  title3: {
    fr: `Concepteur et réalisation : `,
    en: `Design and production : `,
    ru: `Concepteur et réalisation : `
  },
  subtitle3: {
    fr: `Connect IT `,
    en: `Connect IT `,
    ru: `Connect IT `
  },

  subtitle3bis: {
    fr: `– UHDP et`,
    en: `– UHDP et `,
    ru: `– UHDP et `
  },

  subtitle3ter: {
    fr: `Adresses Hotels`,
    en: `Adresses Hotels`,
    ru: `Adresses Hotels`
  },
  title4: {
    fr: `Crédit photo `,
    en: `Photo credit `,
    ru: `Crédit photo `
  },
  subtitle4: {
    fr: `Giulio Ghirardi`,
    en: `Giulio Ghirardi`,
    ru: `Giulio Ghirardi`
  },

  title5: {
    fr: `Droit applicable `,
    en: `Applicable right   `,
    ru: `Droit applicable  `
  },
  texte1: {
    fr: `Les conditions générales de vente s’appliquent à toutes les réservations conclues par internet, via le site www.capdantibes-beachhotel.com. Elles sont régies par la loi française sans faire obstacle aux dispositions impératives protectrices éventuellement applicables du pays de résidence des consommateurs. Le client, préalablement à la commande des services, déclare que la réservation de ces services est effectuée pour ses besoins personnels et déclare avoir la pleine capacité juridique lui permettant de s’engager au titre des présentes conditions générales. Les informations suivantes sont mentionnées au cours de différentes étapes du parcours de réservation : les prix, les modalités de paiement, les conditions générales de vente et les moyens de paiement acceptés. `,
    en: ` The general terms and conditions of sale apply to all bookings made on the internet, via the website www.capdantibes-beachhotel.com/en. They are governed by French law without prejudice to any protective imperative provisions that may be applicable in the country of residence of consumers. The client, prior to ordering the services, declares that the reservation of these services is made for his personal needs and declares that he has full legal capacity to commit himself under these general conditions. The following information is mentioned at different stages of the booking process: prices, payment methods, general terms of sale and accepted means of payment. `,
    ru: `Les conditions générales de vente s’appliquent à toutes les réservations conclues par internet, via le site www.capdantibes-beachhotel.com. Elles sont régies par la loi française sans faire obstacle aux dispositions impératives protectrices éventuellement applicables du pays de résidence des consommateurs. Le client, préalablement à la commande des services, déclare que la réservation de ces services est effectuée pour ses besoins personnels et déclare avoir la pleine capacité juridique lui permettant de s’engager au titre des présentes conditions générales. Les informations suivantes sont mentionnées au cours de différentes étapes du parcours de réservation : les prix, les modalités de paiement, les conditions générales de vente et les moyens de paiement acceptés. `
  },
  title6: {
    fr: `Réservation : `,
    en: `Reservations : `,
    ru: `Réservation : `
  },
  texte2: {
    fr: `Le client reconnaît avoir pris connaissance des modalités de réservation des services disponibles du site www.capdantibes-beachhotel.com et avoir sollicité et obtenu des informations nécessaires et/ou complémentaires pour effectuer sa réservation en parfaite connaissance de cause. Il reste le seul responsable de ses choix en termes de services et de leur adéquation à ses besoins, et nulle responsabilité de l’hôtel ne peut être recherchée à cet égard. La réservation est réputée acceptée par le client à l’issue du processus de réservation. `,
    en: `The customer acknowledges having read the terms and conditions of booking the available services on the www.capdantibes-beachhotel.com/en  website and has requested and obtained the necessary information and / or complementary to make a reservation with full knowledge of the facts. He remains solely responsible for his choices in terms of services and their suitability for his needs, and no liability of the hotel can be sought in this regard. The booking is deemed accepted by the customer at the end of the reservation process. `,
    ru: `Le client reconnaît avoir pris connaissance des modalités de réservation des services disponibles du site www.capdantibes-beachhotel.com et avoir sollicité et obtenu des informations nécessaires et/ou complémentaires pour effectuer sa réservation en parfaite connaissance de cause. Il reste le seul responsable de ses choix en termes de services et de leur adéquation à ses besoins, et nulle responsabilité de l’hôtel ne peut être recherchée à cet égard. La réservation est réputée acceptée par le client à l’issue du processus de réservation. `
  },
  texte3: {
    fr: `Les réservations effectuées par le client se font par l’intermédiaire de notre système de réservation dématérialisé (Fourni par Availpro) et accessible en ligne via le site www.capdantibes-beachhotel.com. Dès réception de la confirmation de réservation, ou lors du prépaiement en ligne par carte bancaire, la réservation est réputée. Le client atteste de la véracité et de l’exactitude des informations transmises. `,
    en: `Bookings made by the customer are made through our cloud-based booking system (Provided by Availpro) and accessible online via the www.capdantibes-beachhotel.com/en. Upon receipt of the booking confirmation, or during online prepayment by credit card, the booking is deemed. The customer certifies the veracity and accuracy of the information transmitted. `,
    ru: `Les réservations effectuées par le client se font par l’intermédiaire de notre système de réservation dématérialisé (Fourni par Availpro) et accessible en ligne via le site www.capdantibes-beachhotel.com. Dès réception de la confirmation de réservation, ou lors du prépaiement en ligne par carte bancaire, la réservation est réputée. Le client atteste de la véracité et de l’exactitude des informations transmises. `
  },
  texte4: {
    fr: `Le Site accuse réception de la réservation du client par l’envoi sans délai d’un courrier électronique au client à l’adresse électronique qu’il a préalablement renseignée. Dans le cas de la réservation en ligne, l’accusé de réception de la réservation par courrier électronique récapitule l’offre de séjour, les services réservés, les prix, les conditions de ventes afférentes au tarif sélectionné, acceptées par le client, la date de réservation effectuée, les informations relatives au service après-vente, ainsi que l’adresse de l’établissement du vendeur auprès duquel le client peut présenter ses réclamations.
Il est rappelé au client, conformément à l’article L. 221-28 du Code de la consommation, qu’il dispose du droit de rétractation prévu à l’article L. 221-18 du Code de la consommation selon les conditions de vente du tarif réservé précisant les modalités d’annulation et/ou de modification de la réservation, lorsque les conditions de vente du tarif réservé le permettent. `,
    en: `The Website acknowledges receipt of the customer’s reservation by sending an e-mail to the customer without delay to the e-mail address that he has previously entered. In the case of online booking, the acknowledgment of receipt of the reservation by e-mail summarizes the offer of stay, the reserved services, the prices, the conditions of sale relating to the selected tariff, accepted by the customer, the date reservation, the after-sales service information, as well as the address of the seller’s establishment where the customer can make claims.
The client is reminded, in accordance with article L. 221-28 of the Consumer Code, that he/she has the right of withdrawal provided for in article L. 221-18 of the Consumer Code according to the conditions of sale of the reserved fare specifying the terms and conditions for cancelling and/or modifying the reservation, when the conditions of sale of the reserved fare allow it.`,
    ru: `Le Site accuse réception de la réservation du client par l’envoi sans délai d’un courrier électronique au client à l’adresse électronique qu’il a préalablement renseignée. Dans le cas de la réservation en ligne, l’accusé de réception de la réservation par courrier électronique récapitule l’offre de séjour, les services réservés, les prix, les conditions de ventes afférentes au tarif sélectionné, acceptées par le client, la date de réservation effectuée, les informations relatives au service après-vente, ainsi que l’adresse de l’établissement du vendeur auprès duquel le client peut présenter ses réclamations.
Il est rappelé au client, conformément à l’article L. 221-28 du Code de la consommation, qu’il dispose du droit de rétractation prévu à l’article L. 221-18 du Code de la consommation selon les conditions de vente du tarif réservé précisant les modalités d’annulation et/ou de modification de la réservation, lorsque les conditions de vente du tarif réservé le permettent. `
  },
  texte5: {
    fr: `l’annulation/modification de la réservation peut s’effectuer directement sur le Site ou directement auprès de l’hôtel.
En cas d’interruption du séjour du fait du client, l’intégralité du prix convenu sera encaissée. Dans le cas de réservation avec prépaiement, aucun remboursement ne sera accordé de ce fait. `,
    en: `The cancellation / modification of the reservation can be done directly on the Site or directly with the hotel.
In case of interruption of the stay due to the customer, the full price agreed upon will be cashed. In the case of reservations with prepayment, no refund will be granted as a result. `,
    ru: `l’annulation/modification de la réservation peut s’effectuer directement sur le Site ou directement auprès de l’hôtel.
En cas d’interruption du séjour du fait du client, l’intégralité du prix convenu sera encaissée. Dans le cas de réservation avec prépaiement, aucun remboursement ne sera accordé de ce fait. `
  },
  texte6: {
    fr: `Le client est informé qu’il doit quitter la chambre avant 12.00 heures le jour de la fin de la réservation. A défaut, il lui sera facturé une nuitée supplémentaire. Toute réservation est nominative et de ce fait personnelle. Elle ne peut en aucun cas être cédée à un tiers, que ce soit à titre gratuit ou onéreux ou à titre commercial. `,
    en: `The customer is informed that he must leave the room before 12.00 am on the day of the end of the reservation. Otherwise, he will be charged an additional night. All bookings are nominative and therefore personal. It can not under any circumstances be transferred to a third party, whether for free or for a fee or for commercial purposes. `,
    ru: `Le client est informé qu’il doit quitter la chambre avant 12.00 heures le jour de la fin de la réservation. A défaut, il lui sera facturé une nuitée supplémentaire. Toute réservation est nominative et de ce fait personnelle. Elle ne peut en aucun cas être cédée à un tiers, que ce soit à titre gratuit ou onéreux ou à titre commercial. `
  },
  texte7: {
    fr: `Lors de l’arrivée du client, il pourra lui être demandé de remplir une fiche de police muni d’une pièce d’identité afin de vérifier son identité. `,
    en: `When the customer arrives, he may be asked to fill out a police card with a piece of identification to verify his identity.`,
    ru: `Lors de l’arrivée du client, il pourra lui être demandé de remplir une fiche de police muni d’une pièce d’identité afin de vérifier son identité. `
  },
  texte8: {
    fr: `En cas d’évènement exceptionnel ou d’impossibilité de mettre la chambre réservée à disposition du client ou en cas de force majeure, l’hôtel se réserve la possibilité de faire héberger totalement ou partiellement le client dans un hôtel de catégorie équivalente, pour des prestations de même nature et sous réserve de l’accord préalable du Client. `,
    en: `In the event of an exceptional event or the impossibility of making the reserved room available to the client or in case of force majeure, the hotel reserves the right to have the guest stay in a hotel of equivalent category, for services of the same nature and subject to the prior consent of the Customer. `,
    ru: `En cas d’évènement exceptionnel ou d’impossibilité de mettre la chambre réservée à disposition du client ou en cas de force majeure, l’hôtel se réserve la possibilité de faire héberger totalement ou partiellement le client dans un hôtel de catégorie équivalente, pour des prestations de même nature et sous réserve de l’accord préalable du Client. `
  },
  texte9: {
    fr: `L’éventuel surcoût de la chambre, le transport entre les deux hôtels et un appel téléphonique restent à la charge de l’hôtel choisi. `,
    en: `The possible additional cost of the room, transportation between the two hotels and a phone call are the responsibility of the chosen hotel. `,
    ru: `L’éventuel surcoût de la chambre, le transport entre les deux hôtels et un appel téléphonique restent à la charge de l’hôtel choisi. `
  },
  texte10: {
    fr: `L’hôtel propose un accès WIFI gratuit permettant aux clients de se connecter à internet. Le client s’engage à ce que les ressources informatiques mises à sa disposition par l’hôtel ne soient en aucune manière utilisées à des fins de reproduction, de représentation, de mise à disposition ou de communication au public d’œuvres ou d’objets protégés par un droit d’auteur ou par un droit voisin, tels que des textes, images, photographies, œuvres musicales, œuvres audiovisuelles, logiciels et jeux vidéo, sans l’autorisation des titulaires des droits prévus aux livres Ier et II du code de la propriété intellectuelle lorsque cette autorisation est requise. Si le client ne se conforme pas aux obligations précitées, il risquerait de se voir reprocher un délit de contrefaçon (article L.335-3 du code de la propriété intellectuelle), sanctionné par une amende de 300.000 euros et de trois ans d’emprisonnement. `,
    en: `The hotel offers free WIFI access for guests to connect to the internet. The customer agrees that the computer resources made available by the hotel are in no way used for purposes of reproduction, representation, provision or communication to the public of works or objects protected by copyright or related rights, such as text, images, photographs, musical works, audiovisual works, software and video games, without the permission of the rights holders set out in Books I and II of the Code of intellectual property when this authorization is required. If the customer does not comply with the aforementioned obligations, he may be accused of an offense of infringement (Article L.335-3 of the Code of Intellectual Property), punishable by a fine of 300,000 euros and three years imprisonment. `,
    ru: `L’hôtel propose un accès WIFI gratuit permettant aux clients de se connecter à internet. Le client s’engage à ce que les ressources informatiques mises à sa disposition par l’hôtel ne soient en aucune manière utilisées à des fins de reproduction, de représentation, de mise à disposition ou de communication au public d’œuvres ou d’objets protégés par un droit d’auteur ou par un droit voisin, tels que des textes, images, photographies, œuvres musicales, œuvres audiovisuelles, logiciels et jeux vidéo, sans l’autorisation des titulaires des droits prévus aux livres Ier et II du code de la propriété intellectuelle lorsque cette autorisation est requise. Si le client ne se conforme pas aux obligations précitées, il risquerait de se voir reprocher un délit de contrefaçon (article L.335-3 du code de la propriété intellectuelle), sanctionné par une amende de 300.000 euros et de trois ans d’emprisonnement. `
  },

  title7: {
    fr: `Règlement des litiges : `,
    en: `Dispute resolution : `,
    ru: `Règlement des litiges : `
  },
  texte11: {
    fr: `Le client est informé que tout litige relatif à l'interprétation ou à l'exécution des présentes mentions légales sera soumis au droit français et aux tribunaux compétents. Le client est également informé de la possibilité de recourir à une procédure de médiation conventionnelle ou à tout autre mode alternatif de règlement des différends.
En cas de litige entre le professionnel et le consommateur, ceux-ci s’efforceront de trouver une solution amiable.
A défaut d’accord amiable, le consommateur a la possibilité de saisir gratuitement le médiateur de la consommation dont relève le professionnel, à savoir le médiateur Tourisme et voyages, dans un délai d’un mois à compter de la réclamation écrite adressée au professionnel.
La saisine du médiateur de la consommation devra s’effectuer : `,
    en: `The customer is informed that any dispute relating to the interpretation or execution of this legal notice shall be subject to French law and to the competent courts. The customer is also informed of the possibility of resorting to a conventional mediation procedure or any other alternative dispute resolution method.
In the event of a dispute between the professional and the consumer, they shall endeavour to find an amicable solution.
In the absence of an amicable agreement, the consumer may refer the matter free of charge to the consumer mediator to which the professional is answerable, namely the Tourism and Travel mediator, within a period of one month from the date of the written complaint addressed to the professional.
The referral to the Consumer Ombudsman must be made : 
`,
    ru: `Le client est informé que tout litige relatif à l'interprétation ou à l'exécution des présentes mentions légales sera soumis au droit français et aux tribunaux compétents. Le client est également informé de la possibilité de recourir à une procédure de médiation conventionnelle ou à tout autre mode alternatif de règlement des différends.
En cas de litige entre le professionnel et le consommateur, ceux-ci s’efforceront de trouver une solution amiable.
A défaut d’accord amiable, le consommateur a la possibilité de saisir gratuitement le médiateur de la consommation dont relève le professionnel, à savoir le médiateur Tourisme et voyages, dans un délai d’un mois à compter de la réclamation écrite adressée au professionnel.
La saisine du médiateur de la consommation devra s’effectuer : `
  },
  texte12: {
    fr: `- soit en complétant le formulaire prévu à cet effet sur le site internet du médiateur Tourisme et voyages : www.mtv.travel `,
    en: `>- either by completing the form provided for this purpose on the Tourism and Travel Ombudsman website: www.mtv.travel`,
    ru: `- soit en complétant le formulaire prévu à cet effet sur le site internet du médiateur Tourisme et voyages : www.mtv.travel `
  },
  texte13: {
    fr: `- soit par courrier adressé à Médiation Tourisme et Voyage BP 80 303 75 823 – Paris Cedex 17. `,
    en: `- or by post addressed to Médiation Tourisme et Voyage BP 80 303 75 823 - Paris Cedex 17. `,
    ru: `- soit par courrier adressé à Médiation Tourisme et Voyage BP 80 303 75 823 – Paris Cedex 17. `
  },
  title8: {
    fr: `Utilisation du site : `,
    en: `Use of the site :`,
    ru: `Utilisation du site : `
  },
  texte14: {
    fr: `La SAS «SOCIETE LES PECHEURS PORT DU CROUTON» s’efforce de mettre à jour le contenu du Site et de délivrer aux Utilisateurs des informations exactes. Toutefois, la SAS «SOCIETE LES PECHEURS PORT DU CROUTON» ne peut garantir l’exactitude, la précision ou l’exhaustivité des données figurant sur le Site. Le SAS «SOCIETE LES PECHEURS PORT DU CROUTON» décline toute responsabilité pour toute inexactitude ou omission portant sur des données disponibles sur le Site ainsi que pour tous dommages résultant d’une intrusion frauduleuse d’un tiers ayant entraîné une modification des dites données. Toute erreur ou omission devra être notifiée à l’adresse suivante : SAS «SOCIETE LES PECHEURS PORT DU CROUTON»   – 4 rue Lamennais – 75008 PARIS – France`,
    en: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON»  endeavours to update the content of the Site and to provide Users with accurate information. However, SAS «SOCIETE LES PECHEURS PORT DU CROUTON» cannot guarantee the accuracy, precision or completeness of the data on the Site. SAS «SOCIETE LES PECHEURS PORT DU CROUTON» shall not be liable for any inaccuracies or omissions in the data available on the Site or for any damage resulting from fraudulent intrusion by a third party leading to a change in such data. Any error or omission must be notified to the following address SAS «SOCIETE LES PECHEURS PORT DU CROUTON»  - 4 rue Lamennais - 75008 PARIS – France`,
    ru: `La SAS «SOCIETE LES PECHEURS PORT DU CROUTON» s’efforce de mettre à jour le contenu du Site et de délivrer aux Utilisateurs des informations exactes. Toutefois, la SAS «SOCIETE LES PECHEURS PORT DU CROUTON» ne peut garantir l’exactitude, la précision ou l’exhaustivité des données figurant sur le Site. Le SAS «SOCIETE LES PECHEURS PORT DU CROUTON» décline toute responsabilité pour toute inexactitude ou omission portant sur des données disponibles sur le Site ainsi que pour tous dommages résultant d’une intrusion frauduleuse d’un tiers ayant entraîné une modification des dites données. Toute erreur ou omission devra être notifiée à l’adresse suivante : SAS «SOCIETE LES PECHEURS PORT DU CROUTON»   – 4 rue Lamennais – 75008 PARIS – France`
  },

  title9: {
    fr: `Accès au site : `,
    en: `Access to the Website: `,
    ru: `Accès au site : `
  },
  texte15: {
    fr: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON» s’efforce de maintenir accessible le Site, sans pour autant être tenue à aucune obligation d’y parvenir. Il est précisé qu’à des fins de maintenance, de mise à jour, et pour toute autre raison notamment d’ordre technique, l’accès au Site pourra être interrompu. La SAS «SOCIETE LES PECHEURS PORT DU CROUTON» n’est en aucun cas responsable de ces interruptions et des conséquences qui peuvent en découler pour l’Utilisateur. `,
    en: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON» endeavours to keep the Site accessible, but is under no obligation to do so. It is specified that access to the Site may be interrupted for maintenance, updating or any other reason, particularly of a technical nature. SAS «SOCIETE LES PECHEURS PORT DU CROUTON» shall in no way be held responsible for these interruptions and the consequences that may result for the User.`,
    ru: `SAS «SOCIETE LES PECHEURS PORT DU CROUTON» s’efforce de maintenir accessible le Site, sans pour autant être tenue à aucune obligation d’y parvenir. Il est précisé qu’à des fins de maintenance, de mise à jour, et pour toute autre raison notamment d’ordre technique, l’accès au Site pourra être interrompu. La SAS «SOCIETE LES PECHEURS PORT DU CROUTON» n’est en aucun cas responsable de ces interruptions et des conséquences qui peuvent en découler pour l’Utilisateur. `
  },

  title10: {
    fr: `Affichage des tarifs et prix : `,
    en: `Display of rates and prices : `,
    ru: `Affichage des tarifs et prix : `
  },

  texte16: {
    fr: `Les prix indiqués sont exprimés par chambre par nombre de personne(s) et à la date sélectionnée. Ils sont confirmés en montant TTC, dans la devise commerciale de l’hôtel, et valables pour la durée indiquée sur le site www.capdantibes-beachhotel.com
Sauf mention contraire sur le site, les prestations complémentaires (petit déjeuner, demi-pension, pension complète…) ne sont pas incluses dans le prix. La taxe de séjour quant à elle est à régler directement sur place auprès de l’hôtel sauf dans le cas d’un prépaiement en ligne où ce montant peut être inclus. Les prix tiennent compte de la TVA applicable au jour de la commande. Toute modification ou instauration de nouvelles taxes légales ou réglementaires imposées par les autorités compétentes seront automatiquement répercutées sur les prix indiqués à la date de la facturation. Lors de la confirmation de la réservation du client, l’hôtel indiquera le montant total de la commande. Le client communique ses coordonnées bancaires à titre de garantie de la réservation sauf conditions ou tarifs spéciaux, par carte bancaire de crédit.
`,
    en: `Prices shown are per room per person (s) and on the selected date. They are confirmed in amount TTC, in the commercial motto of the hotel, and valid for the duration indicated on the site www.capdantibes-beachhotel.com/en.
Unless otherwise stated on the website, the additional services (breakfast, half board, full board, etc.) are not included in the price. The tourist tax is to be paid directly on site with the hotel except in the case of an online prepayment where this amount can be included. The prices take into account the VAT applicable on the day of the order. Any modification or introduction of new legal or regulatory fees imposed by the competent authorities will automatically be reflected in the prices indicated on the date of invoicing. Upon confirmation of the guest’s reservation, the hotel will indicate the total amount of the order. The customer communicates his bank details as a guarantee of the reservation except conditions or special rates, by credit card.
The approximate rates by room category are: 
`,
    ru: `Les prix indiqués sont exprimés par chambre par nombre de personne(s) et à la date sélectionnée. Ils sont confirmés en montant TTC, dans la devise commerciale de l’hôtel, et valables pour la durée indiquée sur le site www.capdantibes-beachhotel.com
Sauf mention contraire sur le site, les prestations complémentaires (petit déjeuner, demi-pension, pension complète…) ne sont pas incluses dans le prix. La taxe de séjour quant à elle est à régler directement sur place auprès de l’hôtel sauf dans le cas d’un prépaiement en ligne où ce montant peut être inclus. Les prix tiennent compte de la TVA applicable au jour de la commande. Toute modification ou instauration de nouvelles taxes légales ou réglementaires imposées par les autorités compétentes seront automatiquement répercutées sur les prix indiqués à la date de la facturation. Lors de la confirmation de la réservation du client, l’hôtel indiquera le montant total de la commande. Le client communique ses coordonnées bancaires à titre de garantie de la réservation sauf conditions ou tarifs spéciaux, par carte bancaire de crédit.
`
  },

  ligne14: {
    fr: `Les tarifs indicatifs par catégories de chambres sont : `,
    en: `Les tarifs indicatifs par catégories de chambres sont : `,
    ru: `Les tarifs indicatifs par catégories de chambres sont : `
  },
  ligne15: {
    fr: `Chambre Deluxe : 1500€`,
    en: `Deluxe room : 1500€`,
    ru: `Chambre Deluxe : 1500€`
  },
  ligne16: {
    fr: `Chambre Privilège : 1800€`,
    en: `Privilège room : 1800€`,
    ru: `Chambre Privilège : 1800€`
  },
  ligne17: {
    fr: `Chambre Privilège vue mer : 2000€`,
    en: `Privilège sea view : 2000€`,
    ru: `Chambre Privilège vue mer : 2000€`
  },
  ligne18: {
    fr: `Suite : 2800€`,
    en: `Suite : 2800€`,
    ru: `Suite : 2800€`
  },
  ligne19: {
    fr: `Suite Le Cap : 4200€`,
    en: `Suite Le Cap : 4200€`,
    ru: `Suite Le Cap : 4200€`
  },
  ligne20: {
    fr: `Chambres communicantes : 3000€`,
    en: `Family rooms : 3000€`,
    ru: `Chambres communicantes : 3000€`
  },
  title11: {
    fr: `Informatique et Libertés : `,
    en: `Data Protection : `,
    ru: `Informatique et Libertés : `
  },
  texte17: {
    fr: `En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.
A l’occasion de l’utilisation du site www.capdantibes-beachhotel.com peuvent être recueillies : l’URL des liens par l’intermédiaire desquels l’utilisateur a accédé au site www.capdantibes-beachhotel.com, le fournisseur d’accès de l’utilisateur, l’adresse de protocole Internet (IP) de l’utilisateur.
En tout état de cause l’Hôtel Cap d’Antibes Beach Hotel ne collecte des informations personnelles relatives à l’utilisateur que pour le besoin de certains services proposés par le site www.capdantibes-beachhotel.com. L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur du site www.capdantibes-beachhotel.com l’obligation ou non de fournir ces informations.
Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d’une copie du titre d’identité avec signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être envoyée.
Aucune information personnelle de l’utilisateur du site www.capdantibes-beachhotel.com n’est publiée à l’insu de l’utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l’hypothèse du rachat de l’Hôtel Cap d’Antibes Beach Hotel de ses droits permettrait la transmission des dites informations à l’éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l’utilisateur du site www.capdantibes-beachhotel.com. Le site n’est pas déclaré à la CNIL car il ne recueille pas d’informations personnelles.
 L'utilisateur qui ne souhaite pas faire l'objet de prospection commerciale par voie téléphonique peut gratuitement s'inscrire sur une liste d'opposition au démarchage téléphonique selon l'Article L223-1 du Code de la consommation. Vous pouvez vous inscrire sur le site internet : https://www.bloctel.gouv.fr
Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données. 
`,
    en: `In France, personal data is protected by French Law no. 78-87 of 6 January 1978, French law no. 2004-801 of 6 August 2004, article L. 226-13 of the French Penal Code and the European Directive of 24 October 1995.
When browsing the website www.capdantibes-beachhotel.com/en, the following data may be collected: the URLs of links from which the user accessed the website www.capdantibes-beachhotel.com/en, details of users’ service providers and the user’s internet protocol (IP) address.
In no event will the Hotel Cap d’Antibes Beach Hotel collect personal data concerning the user except for the use of certain services offered on the website www.capdantibes-beachhotel.com/en. Users knowingly provide this information, notably by entering this information themselves. Users of the website www.capdantibes-beachhotel.com/en are therefore informed of whether or not they are obliged to provide this information.
In accordance with the provisions of article 38 and French Law no.78-17 of 6 January 1978 concerning data protection, all users have the right to access, modify or oppose the use of their personal data by sending a written, signed request, accompanied by a copy of proof of identity with the holder’s signature, specifying the address to which replies should be sent.
No personal information concerning users of the website www.capdantibes-beachhotel.com/en will be published, shared, transferred, ceded or sold in any form whatsoever to third parties without the user’s knowledge. Only the purchase of the Hotel Cap d’Antibes Beach Hotel and its rights would allow this information to be sent to the purchaser, who would then be subject to the same obligations regarding storage and modification of data relating to users of the website www.capdantibes-beachhotel.com/en.
The website is not registered with the CNIL as it does not collect personal data.
The user who does not wish to be the subject of commercial prospecting by telephone can register free of charge on a list of opposition to telephone canvassing according to Article L223-1 of the Consumer Code. You can register on the website: https://www.bloctel.gouv.fr
Databases are protected by the provisions of the French Law of 1 July 1998, modifying the Directive 96/9 of 11 March 1996 relating to the legal protection of databases
.`,
    ru: `En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.
A l’occasion de l’utilisation du site www.capdantibes-beachhotel.com peuvent être recueillies : l’URL des liens par l’intermédiaire desquels l’utilisateur a accédé au site www.capdantibes-beachhotel.com, le fournisseur d’accès de l’utilisateur, l’adresse de protocole Internet (IP) de l’utilisateur.
En tout état de cause l’Hôtel Cap d’Antibes Beach Hotel ne collecte des informations personnelles relatives à l’utilisateur que pour le besoin de certains services proposés par le site www.capdantibes-beachhotel.com. L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur du site www.capdantibes-beachhotel.com l’obligation ou non de fournir ces informations.
Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d’une copie du titre d’identité avec signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être envoyée.
Aucune information personnelle de l’utilisateur du site www.capdantibes-beachhotel.com n’est publiée à l’insu de l’utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l’hypothèse du rachat de l’Hôtel Cap d’Antibes Beach Hotel de ses droits permettrait la transmission des dites informations à l’éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l’utilisateur du site www.capdantibes-beachhotel.com. Le site n’est pas déclaré à la CNIL car il ne recueille pas d’informations personnelles.
 L'utilisateur qui ne souhaite pas faire l'objet de prospection commerciale par voie téléphonique peut gratuitement s'inscrire sur une liste d'opposition au démarchage téléphonique selon l'Article L223-1 du Code de la consommation. Vous pouvez vous inscrire sur le site internet : https://www.bloctel.gouv.fr
Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données. 
`
  },
  title12: {
    fr: `Copyright : `,
    en: `Copyright : `,
    ru: `Copyright : `
  },
  texte18: {
    fr: `Les marques et logos figurant sur ce site sont des marques déposées par l’Hôtel Cap d’Antibes Beach Hotel. Leur mention n’accorde en aucune manière une licence ou un droit d’utilisation quelconque desdites marques, qui ne peuvent donc être utilisées sans le consentement préalable et écrit du propriétaire de la marque sous peine de contrefaçon. L’ensemble des informations présentes sur ce site peut être téléchargé, reproduit, imprimé sous réserve de n’utiliser de telles informations qu’à des fins personnelles et en aucune manière à des fins commerciales ; ne pas modifier de telles informations ; reproduire sur toutes copies la mention des droits d’auteur (“le copyright”) de l’Hôtel Cap d’Antibes Beach Hotel.
Toute autre utilisation non expressément autorisée ou toute représentation totale ou partielle de ce site, par quelque procédé que ce soit, est strictement interdite sans autorisation préalable et écrite de l’Hôtel Cap d’Antibes Beach Hotel et constituerait une contrefaçon sanctionnée par les articles L 355-2 et suivants du Code de la Propriété Intellectuelle.
`,
    en: `Any trademarks or logos used throughout this website are the property of the Hotel Cap d’Antibes Beach Hotel Their inclusion should not be construed as conferring any licence or right to use said trademark and logo, which cannot be used without the prior written permission of their owner in accordance with copyright law. All of the information available on this website can be downloaded, copied and/or printed provided that you: Use said information for private and not commercial purposes; Do not amend said information; Include on all copies the Hotel Cap d’Antibes Beach Hotel copyright policy.
Any other unauthorised use or reproduction in total or in part of this website, by any means whatsoever, without the prior written permission of Hotel Cap d’Antibes Beach Hotel is strictly prohibited and would constitute a copyright infringement punishable by Articles L 355-2 of the French Intellectual Property Code. 
`,
    ru: `Les marques et logos figurant sur ce site sont des marques déposées par l’Hôtel Cap d’Antibes Beach Hotel. Leur mention n’accorde en aucune manière une licence ou un droit d’utilisation quelconque desdites marques, qui ne peuvent donc être utilisées sans le consentement préalable et écrit du propriétaire de la marque sous peine de contrefaçon. L’ensemble des informations présentes sur ce site peut être téléchargé, reproduit, imprimé sous réserve de n’utiliser de telles informations qu’à des fins personnelles et en aucune manière à des fins commerciales ; ne pas modifier de telles informations ; reproduire sur toutes copies la mention des droits d’auteur (“le copyright”) de l’Hôtel Cap d’Antibes Beach Hotel.
Toute autre utilisation non expressément autorisée ou toute représentation totale ou partielle de ce site, par quelque procédé que ce soit, est strictement interdite sans autorisation préalable et écrite de l’Hôtel Cap d’Antibes Beach Hotel et constituerait une contrefaçon sanctionnée par les articles L 355-2 et suivants du Code de la Propriété Intellectuelle.
`
  },
  title13: {
    fr: `Liens hypertextes et cookies : `,
    en: `Hypertext links and cookies : `,
    ru: `Liens hypertextes et cookies : `
  },
  texte19: {
    fr: `Le site www.capdantibes-beachhotel.com contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de l’Hôtel Cap d’Antibes Beach Hotel. Cependant, l’Hôtel Cap d’Antibes Beach Hotel n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.
Lorsque vous vous connectez à notre site, nous pouvons être amenés, sous réserve de vos choix, à installer un ou plusieurs cookies dans votre terminal pendant leur durée légale de validité. Les cookies sont des données stockées dans l’équipement terminal d’un internaute et utilisées par le site émetteur pour envoyer des informations au navigateur de l’internaute, et permettant à ce navigateur de renvoyer des informations au site émetteur.
Conformément à la législation en vigueur, nous vous informons de la finalité des cookies, des moyens de vous y opposer, et du recueil de votre consentement préalable au dépôt de cookies en particulier de mesure d’audience et de publicité par la poursuite de votre navigation sur notre site. 
`,
    en: `The website www.capdantibes-beachhotel.com/en contains a number of hypertext links to other websites, placed with the consent of the Hotel Cap d’Antibes Beach Hotel. Nonetheless, the Hotel Cap d’Antibes Beach Hotel is unable to verify the content of the sites visited and therefore accepts no responsibility for this content.
When you connect to our site, we may, subject to your choice, install one or more cookies in your terminal during their legal validity period. Cookies are data stored in the terminal equipment of a user and used by the sender site to send information to the browser of the user, and allowing the browser to send information to the sender site.
In accordance with the legislation in force, we inform you of the purpose of the cookies, the means of opposing them, and the collection of your consent prior to the deposit of cookies, particularly audience and advertising measurement by the pursuit of your navigation on our site. 
`,
    ru: `Le site www.capdantibes-beachhotel.com contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de l’Hôtel Cap d’Antibes Beach Hotel. Cependant, l’Hôtel Cap d’Antibes Beach Hotel n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.
Lorsque vous vous connectez à notre site, nous pouvons être amenés, sous réserve de vos choix, à installer un ou plusieurs cookies dans votre terminal pendant leur durée légale de validité. Les cookies sont des données stockées dans l’équipement terminal d’un internaute et utilisées par le site émetteur pour envoyer des informations au navigateur de l’internaute, et permettant à ce navigateur de renvoyer des informations au site émetteur.
Conformément à la législation en vigueur, nous vous informons de la finalité des cookies, des moyens de vous y opposer, et du recueil de votre consentement préalable au dépôt de cookies en particulier de mesure d’audience et de publicité par la poursuite de votre navigation sur notre site. 
`
  },

  title14: {
    fr: `Objectif des cookies :`,
    en: `Objective of cookies:`,
    ru: `Objectif des cookies :`
  },

  texte20: {
    fr: `Permettre d’établir des statistiques sur la fréquentation de notre site afin d’améliorer son intérêt et son ergonomie
Faciliter votre navigation sur le site et ainsi adapter nos offres et messages de façon personnalisée (langue utilisée, résolution d’affichage, système d’exploitation etc.) ;
Mémoriser des informations relatives à un formulaire que vous avez rempli sur notre site. Nous ne stockons pas dans les cookies les informations bancaires. Nous sommes également susceptibles de les inclure sur notre site afin de recueillir des données de navigation, mesurer et optimiser l’efficacité de nos campagnes des cookies tiers pour du reciblage publicitaire (retargeting) avec Google analytics.
De plus, des cookies de tiers sont susceptibles d’être émis lors de la réservation (Availpro), ou lors de partage de notre actualité sur certains réseaux sociaux
Nous vous précisons que l’émission et l’utilisation de cookies par des tiers émetteurs sont soumises aux politiques de protection de la vie privée de ces tiers émetteurs. 
`,
    en: `Allow statistics on the attendance of our site to improve its interest and ergonomics
Facilitate your navigation on the site and thus adapt our offers and messages in a personalized way (used language, display resolution, operating system etc.);
Remember information about a form you have filled on our site. We do not store banking information in cookies. We may also include them on our site to collect browsing data, measure and optimize the effectiveness of our third-party cookie campaigns for retargeting with Google analytics.
In addition, third party cookies may be issued when booking (Availpro), or when sharing our news on some social networks
We inform you that the issuing and use of cookies by third-party issuers is subject to the privacy policies of these third-party issuers.
`,
    ru: `Permettre d’établir des statistiques sur la fréquentation de notre site afin d’améliorer son intérêt et son ergonomie
Faciliter votre navigation sur le site et ainsi adapter nos offres et messages de façon personnalisée (langue utilisée, résolution d’affichage, système d’exploitation etc.) ;
Mémoriser des informations relatives à un formulaire que vous avez rempli sur notre site. Nous ne stockons pas dans les cookies les informations bancaires. Nous sommes également susceptibles de les inclure sur notre site afin de recueillir des données de navigation, mesurer et optimiser l’efficacité de nos campagnes des cookies tiers pour du reciblage publicitaire (retargeting) avec Google analytics.
De plus, des cookies de tiers sont susceptibles d’être émis lors de la réservation (Availpro), ou lors de partage de notre actualité sur certains réseaux sociaux
Nous vous précisons que l’émission et l’utilisation de cookies par des tiers émetteurs sont soumises aux politiques de protection de la vie privée de ces tiers émetteurs. 
`
  },
  title15: {
    fr: `Newsletter : `,
    en: `Newsletter : `,
    ru: `Newsletter : `
  },
  texte21: {
    fr: `Si vous vous abonnez sur notre site pour recevoir nos newsletters, nous vous adresserons par e-mail des informations susceptibles de vous intéresser. Afin de mieux répondre à vos besoins, nous mesurons les taux de clics sur les liens intégrés dans les newsletters. Le prestataire qui gère l’envoi des newsletters n’a pas le droit d’exploiter les informations personnelles qu’il détient. Si vous souhaitez interrompre votre abonnement, vous pouvez à tout moment vous désabonner à partir de nos newsletters en cliquant sur le lien prévu à cet effet en bas de page. Si vous êtes inscrit sur notre site, vous pouvez à tout moment vous désabonner en modifiant vos informations personnelles après vous être identifié. `,
    en: `If you register on our website to receive our newsletters, we will email you with information that may interest you. In order to better meet your requirements, we measure the number of clicks on links incorporated into the newsletters. The service provider managing the sending of newsletters does not have the right to use the personal information that it holds. If you wish to put an end to your subscription, you can unsubscribe at any time by clicking on the relevant link at the bottom of the newsletter page. If you are registered on our website, you can unsubscribe at any time by changing your personal information after logging in.
Applicable law and jurisdiction
Any disputes relating to use of the www.capdantibes-beachhotel.com/en website are subject to French law. The relevant courts of Paris shall have exclusive jurisdiction.
Principle laws concerned
French Law no. 78-17 of 6 January 1978, notably modified by the Law no. 2004-801 of 6 August 2004 concerning data protection. 
`,
    ru: `Si vous vous abonnez sur notre site pour recevoir nos newsletters, nous vous adresserons par e-mail des informations susceptibles de vous intéresser. Afin de mieux répondre à vos besoins, nous mesurons les taux de clics sur les liens intégrés dans les newsletters. Le prestataire qui gère l’envoi des newsletters n’a pas le droit d’exploiter les informations personnelles qu’il détient. Si vous souhaitez interrompre votre abonnement, vous pouvez à tout moment vous désabonner à partir de nos newsletters en cliquant sur le lien prévu à cet effet en bas de page. Si vous êtes inscrit sur notre site, vous pouvez à tout moment vous désabonner en modifiant vos informations personnelles après vous être identifié. `
  },
  title16: {
    fr: `Droit applicable et attribution de juridiction : `,
    en: `Applicable law and jurisdiction : `,
    ru: `Droit applicable et attribution de juridiction : `
  },
  texte22: {
    fr: `Tout litige en relation avec l’utilisation du site www.capdantibes-beachhotel.com est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Paris. `,
    en: `Any dispute relating to the use of the www.capdantibes-beachhotel.com/en website is subject to French law. The competent courts of Paris have exclusive jurisdiction. `,
    ru: `Tout litige en relation avec l’utilisation du site www.capdantibes-beachhotel.com est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Paris. `
  },
  title17: {
    fr: `Exercice du droit de rétractation `,
    en: `Exercising the right of withdrawal : `,
    ru: `Exercice du droit de rétractation `
  },
  texte23: {
    fr: `Nous informons nos clients que d’après l'article L221-28 du Code de la consommation, l'exercice du droit de rétractation s'applique aux prestations de services d'hébergement selon les conditions d’annulation choisies lors de la réservation.
En conséquence, seule certaines prestations de services de l’hébergement peuvent être modifiées ou annulées sans l’accord préalable de l’hôtel. Nous attirons l'attention de nos clients sur le fait que toute réservation non-annulable entraîne l'engagement de verser l'intégralité du montant de la prestation
Copyright © SAS «SOCIETE LES PECHEURS PORT DU CROUTON» 
`,
    en: ``,
    ru: `Nous informons nos clients que d’après l'article L221-28 du Code de la consommation, l'exercice du droit de rétractation s'applique aux prestations de services d'hébergement selon les conditions d’annulation choisies lors de la réservation.
En conséquence, seule certaines prestations de services de l’hébergement peuvent être modifiées ou annulées sans l’accord préalable de l’hôtel. Nous attirons l'attention de nos clients sur le fait que toute réservation non-annulable entraîne l'engagement de verser l'intégralité du montant de la prestation
Copyright © SAS «SOCIETE LES PECHEURS PORT DU CROUTON» 
`
  },
  title18: {
    fr: `Les principales lois concernées : `,
    en: `Les principales lois concernées : `,
    ru: `Les principales lois concernées : `
  },
  texte24: {
    fr: `Loi n° 78-17 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l’informatique, aux fichiers et aux libertés
Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique
`,
    en: `We inform our clients that according to article L221-28 of the Consumer Code, the exercise of the right of withdrawal applies to accommodation services according to the cancellation conditions chosen at the time of booking.
Consequently, only certain accommodation services may be modified or cancelled without the prior agreement of the hotel. We would like to point out to our guests that any non-cancellable reservation implies a commitment to pay the full amount of the service.

Copyright © SAS «SOCIETE LES PECHEURS PORT DU CROUTON» 

The main laws involved:
Law No. 78-17 of 6 January 1978, as amended by Law No. 2004-801 of 6 August 2004 on information technology, files and freedoms
Law n° 2004-575 of 21 June 2004 for confidence in the digital economy

`,
    ru: `Loi n° 78-17 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l’informatique, aux fichiers et aux libertés
Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique
`
  }
}
