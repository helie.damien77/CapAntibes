import Languages from '@/enums/languages'
import Mentions from '@/features/mentions'

export async function getStaticProps() {
  return {
    props: {
      language: Languages.FR
    }
  }
}

export default function MentionPage() {
  return (
    <Mentions
      title={`Mentions légales | Hôtel du cap d'antibes - Côte d'azur`}
      description={`Les mentions légales de l'hôtel du cap d'antibes beach hotel, un hotel de charme sur la côte d'Azur`}
    />
  )
}
