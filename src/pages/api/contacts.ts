import { NextApiRequest, NextApiResponse } from 'next'
import * as SibApiV3Sdk from '@sendinblue/client'

const apiInstance = new SibApiV3Sdk.ContactsApi()

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const API_KEY = process.env.BREVO_API_KEY

  if (API_KEY === undefined) {
    return res
      .status(500)
      .json({ msg: 'Something went wrong while accessing Brevo' })
  }

  apiInstance.setApiKey(SibApiV3Sdk.ContactsApiApiKeys.apiKey, API_KEY)

  const { method } = req

  if (method === 'POST') {
    const body = req.body

    const createContact = new SibApiV3Sdk.CreateContact()

    createContact.email = body.mail
    createContact.listIds = [body.listId]

    console.log(createContact)

    apiInstance.createContact(createContact).then(
      function (data) {
        console.log(
          'API called successfully. Returned data: ' + JSON.stringify(data)
        )
        res.status(200).json(JSON.stringify(data))
      },
      function (error) {
        console.error(error)
      }
    )
  } else {
    res.setHeader('Allow', ['POST'])
    res.status(405).end(`Method ${method} Not Allowed`)
  }
}
