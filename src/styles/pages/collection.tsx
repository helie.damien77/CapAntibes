import { createGlobalStyle } from 'styled-components'

export const CollectionPageStyles = createGlobalStyle`
  main#collection {
    a {
      display: flex;
    }
    p, .btn {
      line-height: 24px;
    }
    .p {
      font-size: 17px;
      p {
        text-transform: none;
      }
    }
    .btn {
      margin: 0;
      text-transform: uppercase;
      font-size: 16px;
      p, svg {
        height: 22px;
      }
    }
    #address-btn {
      height: 17px;
      width: auto;
    }
    &.mobile {
      p, .btn {
        line-height: 21px;
        p, svg {
          height: 23px;
        }
        p {
          padding: 3px 0;
        }
      }
      .p {
        font-size: 13px;
        p {
          text-transform: none;
        }
      }
      .btn {
        font-size: 13px;
      }
      #address-btn {
        height: 16px;
        width: auto;
      }
    }
  }
`
