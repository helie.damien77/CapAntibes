import { createGlobalStyle } from 'styled-components'

export const RoomPageStyles = createGlobalStyle`
  main#room {
      margin-top: 80px;
    .h3, .p, .btn, .menu, .item {
      line-height: 24px;
    }
    .h3 {
      font-size: 24px;
      padding-bottom: 1.8vw;
    }
    .btn {
      margin-top: 15px;
      text-transform: uppercase;
      font-size: 16px;
      p, svg {
        height: 22px;
      }
    }
    .p {
      font-size: 17px;
      p {
        text-transform: none;
      }
    }
    .menu {
      font-size: 11px;
    }
    .item {
      font-size: 11px;
      margin-bottom: initial !important;
    }
    .sc-column :first-child {
      margin-bottom: initial !important;
    }
    .menu-middle p {
      line-height: normal;
      border-bottom: 2px solid ${(props) => props.theme.fontColors.primary};
      padding-bottom: -5px;
    }

    .section .btn {
      margin-left: 5%
    }

    &.mobile {
      img {
        height: auto;
      }
    .h3, .p, .btn, .menu, .item {
        line-height: 21px;
      }
      .h3 {
        font-size: 20px;
        margin-bottom: calc(-1.8vw + 15px) !important
      }
      .btn {
        font-size: 13px;
        margin-top : 0px;
        p, svg {
          height: 23px;
        }
        p {
          padding: 3px 0;
        }
      }
      .p {
        font-size: 13px;
      }
      .menu {
        font-size: 13px;
      }
      .item {
        font-size: 10px;
        margin-bottom: initial !important;
      }

      .menu > a {
        margin: 5px 0;
      }

      .menu-middle p {
      border-bottom: 2px solid ${(props) => props.theme.fontColors.primary};
    }
      .section p {
        margin: 5% 0;
      }
      .section .btn {
        margin-top: 2%;
        margin-left: inherit;
      }
    }
  }
`
