import { createGlobalStyle } from 'styled-components'

export const IndexPageStyles = createGlobalStyle`
  main#home {
    margin-top: 85px;
    .first-line {
      margin-bottom: 10px;
    }
    .icon-fish {
      width: auto;
    }
    .h3, .h6, p, .btn {
      line-height: 24px;
    }
    .h3 {
      text-transform: uppercase;
      font-size: 24px;
      img {
        padding-left: 20px;
      }
    }
    .h3:not(.without-margin) {
      margin-bottom: 20px;
    }
    .h3.without-margin {
      margin-bottom: 0px;
    }
    .h6 {
      text-transform: uppercase;
      font-size: 17px;
      margin-top: -20px;
      margin-bottom: 20px;
    }
    .btn {
      margin-top: 15px;
      text-transform: uppercase;
      font-size: 16px;
      p, svg {
        height: 22px;
      }
    }
    .p {
      font-size: 17px;
      p {
        text-transform: none;
      }
    }
    .yellow {
      border: 3px solid #FAB50B;
    }
    &.mobile {
      .h3, .h6, p, .btn {
        line-height: 21px;
      }
      .h3 {
        font-size: 20px;
        margin-bottom: 15px;
      }
      .h3:not(.without-margin) {
        margin-bottom: 15px;
      }
      .h3.without-margin {
        margin-bottom: 0px;
      }
      .h6 {
        font-size: 13px;
        margin-top: -15px;
        margin-bottom: 15px;
      }
      .btn {
        margin-top: 10px;
        font-size: 14px;
        p, svg {
          height: 23px;
        }
        p {
          padding: 3px 0;
        }
      }
      .p {
        font-size: 13px;
        p {
          text-transform: none;
        }
      }
      .yellow {
        margin-top: 42px;
      }
    }
  }
`
