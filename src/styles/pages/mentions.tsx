import { createGlobalStyle } from 'styled-components'

export const MentionsPageStyles = createGlobalStyle`
  main#mentions {
      margin-top: 80px;
      .h1, .h2, .h3, p, .p2 {
      line-height: 24px;
    }
      .h1 {
      font-size: 24px;
      margin-bottom: 8%;
      text-transform: uppercase;
      margin: auto;
      padding: 1.8vw 0vw;
    }
    .p, .p2 {
      text-transform: initial;
      margin: auto;
    }

    .p2{
      font-size: 16px;
      padding: 20px 0px;
    }

     .h2{
      margin: auto;
      font-size: 15px;
     }

    .t{
      width: 80%;
      margin: auto;
      padding-bottom:20px;
      text-align: center !important;
      }
     }

    &.mobile {
      .h1 {
        font-size: 16px !important;
        padding: 1.8vw 0vw;
        text-align: center;
    }
    .h2 {
        font-size: 13px !important;
        padding: 1.8vw 0vw;
        text-align: center !important;
    }
    }
  }
`
