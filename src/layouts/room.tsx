import RowToColumn from '@/components/RowToColumn'
import { useIsLaptop, useLink, useText } from '@/hooks'
import { IContentByLang } from '@/interfaces'
import { RoomPageStyles } from '@/styles/pages/rooms/room'
import Head from 'next/head'
import Image, { StaticImageData } from 'next/image'
import Link from 'next/link'
import Text from '@/components/Text'
import Button from '@/components/Button'
import Column from '@/components/Column'
import Row from '@/components/Row'
import ExternalLink from '@/components/ExternalLink'

interface IITemProps {
  itemPadding: number
  item: IContentByLang
}
const Item = ({ itemPadding, item }: IITemProps) => {
  const itemText = useText(item)
  const isLaptop = useIsLaptop()
  return (
    <Column
      style={{
        marginTop: `${2 * itemPadding}px`,
        paddingBottom: '10px',
        borderBottom: '1px rgb(217, 155, 129) solid'
      }}
      opt_spacing={{ unit: 'px', value: itemPadding }}
    >
      <Text className="item">{itemText}</Text>
    </Column>
  )
}

interface IMeta {
  title: string
  description: string
}
interface IMenu {
  left: string
  middle: string
  right: string
}
interface IRoomLayoutProps {
  meta: IMeta
  content: Record<string, IContentByLang>
  items: IContentByLang[]
  menu: IMenu
  toBookLink: string
  images: StaticImageData[]
  imagesMobile: StaticImageData[]
}
const RoomLayout = (props: IRoomLayoutProps): JSX.Element => {
  const { meta, content, menu, toBookLink, images, imagesMobile, items } = props
  const isLaptop = useIsLaptop()
  const imgsToShow = isLaptop ? images : imagesMobile
  const nbItemsPerColumn = Math.round(items.length / 3)
  const itemsColumnMargin = isLaptop ? '25px' : 'initial'
  const itemPadding = isLaptop ? 6 : 3

  const renderItemsColumn = (start: number) => {
    const end = nbItemsPerColumn + start
    const _items = items.slice(start, end)
    return _items.map((item, index) => (
      <Item key={start + index} item={item} itemPadding={itemPadding} />
    ))
  }
  const renderItems = () => (
    <>
      <Column>{renderItemsColumn(0)}</Column>
      <Column
        style={{
          marginRight: itemsColumnMargin,
          marginLeft: itemsColumnMargin
        }}
      >
        {renderItemsColumn(nbItemsPerColumn)}
      </Column>
      <Column>{renderItemsColumn(2 * nbItemsPerColumn)}</Column>
    </>
  )

  return (
    <>
      <RoomPageStyles />
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main id="room" className={!isLaptop ? 'mobile' : ''}>
        <Row>
          {isLaptop ? (
            <>
              <Image
                style={{ width: '50%', height: 'auto' }}
                src={imgsToShow[0]}
                alt="image"
              />
              <Image
                style={{ width: '50%', height: 'auto' }}
                src={imgsToShow[1]}
                alt="image"
              />
            </>
          ) : (
            <Image src={imgsToShow[0]} alt="image" />
          )}
        </Row>
        <RowToColumn
          className="menu"
          align="center"
          marginForRow={[0.2, 0.5, 0.5, 0.5]}
          marginForColumn={[1, 0.5, 1, 0.5]}
        >
          <Column opt_alignItems="flex-start">
            <Link
              shallow
              style={{
                display: 'flex',
                justifyContent: 'flex-start'
              }}
              href={useLink(menu.left)}
            >
              <Button opt_arrow_position="left">
                <Text>{useText(content.menuLeft)}</Text>
              </Button>
            </Link>
          </Column>
          {/* <Link
          shallow
            className="menu-middle"
            style={{ display: 'flex', justifyContent: 'center' }}
            href={useLink(menu.middle)}
          >
            <Text>{useText(content.menuMiddle)}</Text>
          </Link> */}

          <Column opt_alignItems="flex-end">
            <Link
              shallow
              style={{
                display: 'flex',
                justifyContent: 'flex-end'
              }}
              href={useLink(menu.right)}
            >
              <Button opt_arrow_position="right">
                <Text>{useText(content.menuRight)}</Text>
              </Button>
            </Link>
          </Column>
        </RowToColumn>
        <Column
          opt_margin={isLaptop ? [0.5, 1, 0.26, 1] : [1, 0.5, 1, 0.5]}
          opt_spacing={{ unit: 'px', value: 10 }}
          style={{ width: `${isLaptop ? '80%' : '100%'}` }}
        >
          <Text className="h3">{useText(content.title)}</Text>
          <Text className="p">{useText(content.subTitle1)}</Text>
          <ExternalLink link={toBookLink}>
            <Button className="btn">
              <Text>{useText(content.toBook)}</Text>
            </Button>
          </ExternalLink>
        </Column>
        <RowToColumn
          className="items"
          align="flex-start"
          marginForRow={[0.5, 1, 0.5, 1]}
          marginForColumn={[1, 0.5, 1, 0.5]}
        >
          {renderItems()}
        </RowToColumn>
        <Row opt_margin={[0, 0, 0.6, 0]}>
          <Image
            src={isLaptop ? imgsToShow[2] : imgsToShow[1]}
            className={'fullWidth'}
            alt={`images de ${useText(content.title)}`}
          />
        </Row>
      </main>
    </>
  )
}

export default RoomLayout
