import { contentsToBook } from '@/contents/globals'
import { useIsLaptop, useText } from '@/hooks'
import { LangContext, MenuContext } from '@/stores'
import { useContext, useEffect, useState } from 'react'
import * as S from './styles'
import Row from '../Row'
import Column from '../Column'
import Image from 'next/image'
import img1 from '/public/assets/images/rooms/tobook/tobookroommenu.jpg'
import Divider from '../Divider'
import Text from '../Text'
import Button from '../Button'
import { useRouter } from 'next/router'
import Input from '../Input'
import { DayPicker, SelectSingleEventHandler } from 'react-day-picker'
import { fr, enGB, ru } from 'date-fns/locale'
import Languages from '@/enums/languages'

const ToBookRoomMenu = () => {
  const router = useRouter()
  const { lang } = useContext(LangContext)
  const { menuOpen } = useContext(MenuContext)
  const [isDateExpanded, setIsDateExpanded] = useState(false)
  const [isArrivalDateSelected, setIsArrivalDateSelected] = useState(true)
  const [menuStatus, setMenuStatus] = useState<'close' | 'open' | 'undefined'>(
    'undefined'
  )
  const [adultsValue, setAdultsValue] = useState<number>(2)
  const [childrenValue, setChildrenValue] = useState<number>(0)
  const [codeValue, setCodeValue] = useState<string>('')
  const today = new Date()
  const tomorrow = new Date(new Date().setDate(today.getDate() + 1))
  const [arrivalValue, setArrivalValue] = useState<Date>(today)
  const [departureValue, setDepartureValue] = useState<Date>(tomorrow)
  const isLaptop = useIsLaptop()
  const inputSize = isLaptop ? '16px' : '13px'

  const labels = {
    title: useText(contentsToBook.title),
    title_mobile: useText(contentsToBook.title_mobile),
    period: useText(contentsToBook.period),
    period_arrival: useText(contentsToBook.period_arrival),
    period_departure: useText(contentsToBook.period_departure),
    adults: useText(contentsToBook.adults),
    children: useText(contentsToBook.children),
    code: useText(contentsToBook.code),
    button: useText(contentsToBook.button)
  }
  let dateLocale = fr
  if (lang === Languages.EN) dateLocale = enGB
  if (lang === Languages.RU) dateLocale = ru

  useEffect(() => {
    switch (menuOpen) {
      case 'tobook-room':
        setMenuStatus('open')
        break
      case 'tobook-room-close':
        setMenuStatus('close')
        break
      default:
        setMenuStatus('undefined')
        break
    }
  }, [menuOpen])

  const seePrice = (event: any) => {
    event.preventDefault()
    const linkPath = `https://www.secure-hotel-booking.com/Cap-d-Antibes-Beach-Hotel/JK7H/fr?hotelId=13829&selectedAdultCount=${adultsValue}&selectedChildCount=${childrenValue}&arrivalDate=${arrivalValue.toLocaleDateString()}&departureDate=${departureValue.toLocaleDateString()}&promocode=${codeValue}`
    const newWindow = window.open(linkPath, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  const handleDepartureDateClick = () => {
    setIsArrivalDateSelected(false)
    setIsDateExpanded(true)
  }
  const handleArrivalDateClick = () => {
    setIsArrivalDateSelected(true)
    setIsDateExpanded(true)
  }
  const handleDepartureDayClick = (day: Date) => {
    setDepartureValue(day)
    setIsDateExpanded(false)
  }
  const handleArrivalDayClick = (day: Date) => {
    setArrivalValue(day)
    setIsDateExpanded(false)
  }
  const handleArrowClick = () => {
    setIsDateExpanded(!isDateExpanded)
  }
  const handleAdultsChange = (event: any) => {
    setAdultsValue(event.target.value)
  }
  const handleChildrenChange = (event: any) => {
    setChildrenValue(event.target.value)
  }
  const handleCodeChange = (event: any) => {
    setCodeValue(event.target.value)
  }

  return (
    <S.Wrapper
      data-open={menuStatus}
      className={`sc-tobookroommenu ${!isLaptop ? 'mobile' : ''}`}
    >
      <Row opt_margin={[0, 0.5, 1, 0.5]}>
        <Column
          className="form-column"
          opt_margin={isLaptop ? [0, 0.5, 0, 0] : [0, 0.5, 0, 0.5]}
        >
          <Row opt_margin={[0, 0, 0, 0]}>
            <Text className="h1" opt_align={isLaptop ? 'left' : 'center'}>
              {isLaptop ? labels.title : labels.title_mobile}
            </Text>
          </Row>
          <form onSubmit={seePrice}>
            <Row opt_margin={[0, 0, 0, 0]}>
              <Column className="list">
                <Divider opt_height={{ unit: 'px', value: 2 }} />
                {isLaptop && (
                  <>
                    <Row className="item-input" opt_margin={[0, 0, 0, 0]}>
                      <Text>{labels.period}</Text>
                      <Row
                        className="date-view"
                        opt_margin={[0, 0, 0, 0]}
                        opt_justifyContent="space-between"
                        data-isexpanded={isDateExpanded}
                      >
                        <div
                          onClick={handleArrowClick}
                          className="date-container"
                        >
                          <Text className="date-input date-preview">
                            {arrivalValue
                              ? arrivalValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                        </div>
                        <div
                          onClick={handleArrowClick}
                          className="date-container"
                        >
                          <Text className="date-input date-preview">
                            {departureValue
                              ? departureValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                        </div>
                        <div
                          className="date-arrow"
                          onClick={handleArrowClick}
                          data-isopen={isDateExpanded}
                        >
                          <svg
                            className="svg-arrow-bottom"
                            width="16"
                            height="13"
                            viewBox="0 0 16 13"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M1 0.999939L8 10.9999L11.5 5.99994L15 0.999939"
                              stroke="#D99380"
                              strokeWidth="2"
                            />
                          </svg>
                          <svg
                            className="svg-arrow-top"
                            width="16"
                            height="15"
                            viewBox="0 0 16 15"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M1 0.999939L8 10.9999L11.5 5.99994L15 0.999939"
                              stroke="#D99380"
                              strokeWidth="2"
                            />
                          </svg>
                        </div>
                      </Row>
                    </Row>
                    {isDateExpanded && (
                      <Row className="date-selection" opt_margin={[0, 0, 0, 0]}>
                        <Column
                          className="date-section"
                          opt_margin={[0, 0.3, 0, 0]}
                        >
                          <Text>{labels.period_arrival}</Text>
                          <Text className="date-input">
                            {arrivalValue
                              ? arrivalValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                          <DayPicker
                            className="date-picker"
                            mode="single"
                            locale={dateLocale}
                            selected={arrivalValue}
                            onSelect={
                              setArrivalValue as SelectSingleEventHandler
                            }
                            modifiersStyles={{
                              selected: { color: '#F3D7C8' }
                            }}
                            defaultMonth={arrivalValue}
                            fromDate={today}
                          />
                        </Column>
                        <Column
                          className="date-section"
                          opt_margin={[0, 0, 0, 0.3]}
                        >
                          <Text>{labels.period_departure}</Text>
                          <Text className="date-input">
                            {departureValue
                              ? departureValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                          <DayPicker
                            className="date-picker"
                            mode="single"
                            locale={dateLocale}
                            selected={departureValue}
                            onDayClick={(e) => {
                              setDepartureValue(e)
                              setIsDateExpanded(false)
                            }}
                            modifiersStyles={{
                              selected: { color: '#F3D7C8' }
                            }}
                            defaultMonth={departureValue}
                            fromDate={tomorrow}
                          />
                        </Column>
                      </Row>
                    )}{' '}
                  </>
                )}
                {!isLaptop && (
                  <>
                    <Row className="item-input" opt_margin={[0, 0, 0, 0]}>
                      <Text>{labels.period}</Text>
                      <Row
                        className="date-view"
                        opt_margin={[0, 0, 0, 0]}
                        opt_justifyContent="space-between"
                      >
                        <div
                          onClick={handleArrivalDateClick}
                          style={{ width: 'fit-content' }}
                        >
                          <Text className="date-input date-preview">
                            {arrivalValue
                              ? arrivalValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                        </div>
                        <div
                          onClick={handleDepartureDateClick}
                          style={{ width: 'fit-content' }}
                        >
                          <Text className="date-input date-preview">
                            {departureValue
                              ? departureValue
                                  .toLocaleDateString()
                                  .replaceAll('/', ' • ')
                              : ''}
                          </Text>
                        </div>
                      </Row>
                    </Row>
                    {isDateExpanded && (
                      <Row className="date-selection" opt_margin={[0, 0, 0, 0]}>
                        {isArrivalDateSelected ? (
                          <DayPicker
                            className="date-picker"
                            mode="single"
                            locale={dateLocale}
                            selected={arrivalValue}
                            onDayClick={handleArrivalDayClick}
                            modifiersStyles={{
                              selected: { color: '#F3D7C8' }
                            }}
                            defaultMonth={arrivalValue}
                            fromDate={today}
                          />
                        ) : (
                          <DayPicker
                            className="date-picker"
                            mode="single"
                            locale={dateLocale}
                            selected={departureValue}
                            onDayClick={handleDepartureDayClick}
                            modifiersStyles={{
                              selected: { color: '#F3D7C8' }
                            }}
                            defaultMonth={departureValue}
                            fromDate={tomorrow}
                          />
                        )}
                      </Row>
                    )}{' '}
                  </>
                )}
                <Divider opt_height={{ unit: 'px', value: 2 }} />
                <Row className="item-input" opt_margin={[0, 0, 0, 0]}>
                  <Text>{labels.adults}</Text>
                  <Input
                    min={0}
                    type="number"
                    opt_fontSize={inputSize}
                    value={adultsValue}
                    onChange={handleAdultsChange}
                  />
                </Row>
                <Divider opt_height={{ unit: 'px', value: 2 }} />
                <Row className="item-input" opt_margin={[0, 0, 0, 0]}>
                  <Text>{labels.children}</Text>
                  <Input
                    min={0}
                    type="number"
                    opt_fontSize={inputSize}
                    value={childrenValue}
                    onChange={handleChildrenChange}
                  />
                </Row>
                <Divider opt_height={{ unit: 'px', value: 2 }} />
                <Row className="item-input" opt_margin={[0, 0, 0, 0]}>
                  <Text>{labels.code}</Text>
                  <Input
                    maxLength={50}
                    type="text"
                    opt_fontSize={inputSize}
                    value={codeValue}
                    onChange={handleCodeChange}
                  />
                </Row>
                <Divider opt_height={{ unit: 'px', value: 2 }} />
              </Column>
            </Row>
            <Row opt_margin={[2, 0, 0, 0]}>
              <Button
                type="submit"
                className="btn"
                style={{ justifyContent: isLaptop ? 'flex-start' : 'center' }}
              >
                <Text>{labels.button}</Text>
              </Button>
            </Row>
          </form>
        </Column>
        {isLaptop && (
          <Column
            opt_margin={[0, 0, 0, 1]}
            opt_alignItems="center"
            opt_justifyContent="flex-start"
            className="image"
          >
            <Image src={img1} alt="vue sur mers" />
          </Column>
        )}
      </Row>
    </S.Wrapper>
  )
}

export default ToBookRoomMenu
