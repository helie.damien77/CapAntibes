import React from 'react'
import styled, { keyframes } from 'styled-components'

const fadeOut = keyframes`
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
`
const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`

const slideLeft = keyframes`
  from {
    transform: translateX(100%);
  }

  to {
    transform: translateX(0%);
  }
`
const slideRight = keyframes`
  from {
    transform: translateX(0%);
  }

  to {
    transform: translateX(100%);
  }
`

export const Wrapper = styled.div`
  &.mobile {
    transform: translateX(100%);
    display: flex;

    .form-column {
      width: 100%;
    }
  }
  &[data-open='open'].mobile {
    animation: ${slideLeft} 1s forwards ease-in-out;
  }

  &[data-open='close'].mobile {
    animation: ${slideRight} 1s forwards ease-in-out;
  }

  &[data-open='open']:not(.mobile) {
    display: flex;
  }

  &[data-open='close']:not(.mobile) {
    display: none;
  }

  display: none;
  position: fixed;
  top: 0;
  z-index: 3;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: ${(props) => props.theme.bgColors.primary};
  width: 100vw;
  height: 100%;
  color: ${(props) => props.theme.fontColors.primary};
  font-family: ${(props) => props.theme.fontStyles.medium};

  margin-top: ${(props) => props.theme.navBar.laptop}px;
  padding-top: 5.5%;
  &.mobile {
    padding-top: 20%;
    margin-top: ${(props) => props.theme.navBar.laptop}px;
    height: calc(100% - 85px);
    z-index: 20;
  }

  .image {
    height: 65vh !important;
  }

  img {
    margin-bottom: 0px !important;
    height: inherit;
    width: 100%;
    object-fit: cover;
  }
  @media screen and (max-width: 1380px) {
    .date-preview p {
      width: auto !important;
      font-size: 12px;
    }
  }

  &.mobile .sc-divider {
    height: 1px;
  }
  .form-column {
    overflow-y: scroll;
    padding-bottom: 80px;
    height: calc(100vh - 84px - 5.5%);
  }

  .form-column::-webkit-scrollbar {
    display: none;
  }

  .date-view {
    display: flex;
    height: 100%;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  }

  .item-input {
    padding: 7px 0px;
  }

  .item-input > *:first-child {
    flex: 1.7 !important;
  }

  .date-preview {
    border-bottom: none !important;
  }

  .date-container {
    width: auto !important;
  }

  .date-selection .date-section > *:first-child {
    margin-bottom: 6px !important;
  }

  &.mobile .btn div {
    border-bottom: 1px solid #d99b81 !important;
  }

  .btn div {
    border-bottom: 2px solid #d99b81;
  }
  .btn div p {
    font-size: 22px;
  }
  .btn svg {
    display: none;
  }

  .rdp {
    margin-top: 25px !important;
    margin-bottom: 25px !important;
  }

  &.mobile .rdp {
    margin-top: 0px !important;
    margin-bottom: 15px !important;
  }

  .sc-input,
  .date-input {
    border-bottom: 2px solid #d99b81 !important;
  }

  &.mobile .sc-input,
  &.mobile .date-input {
    border-bottom: 1px solid #d99b81 !important;
  }

  form {
    width: 100%;
    * {
      font-size: 16px;
      font-family: ${(props) => props.theme.fontStyles.medium};
      font-weight: 400;
      color: ${(props) => props.theme.fontColors.primary};
    }

    .date-picker * {
      font-size: 11px;
    }
  }
  &.mobile form * {
    font-size: 11px;
  }

  &.mobile .btn div p {
    font-size: 13px;
  }

  .date-input {
    width: fit-content !important;
    padding-bottom: 2px;
    border-bottom: 1px solid ${(props) => props.theme.fontColors.primary};
    * {
      width: fit-content !important;
    }
  }
  [data-isexpanded='true'] .date-preview {
    opacity: 0;
    cursor: none;
  }
  [data-isexpanded='false'] .date-preview {
    opacity: 1;
    cursor: pointer;
  }
  .date-arrow {
    width: fit-content;
    > .svg-arrow-bottom {
      height: 20px;
      padding-top: 5px;
    }
    > .svg-arrow-top {
      transform: scaleY(-1);
      height: 20px;
    }

    > *:first-child {
      opacity: 1;
      width: 100%;
    }
    > *:last-child {
      opacity: 0;
      width: 0%;
    }

    &[data-isopen='false'] > * {
      :first-child {
        width: 100%;
        animation: ${fadeIn} 0.2s forwards ease-in-out;
      }
      :last-child {
        width: 0%;
        animation: ${fadeOut} 0.2s forwards ease-in-out;
      }
    }
    &[data-isopen='true'] > * {
      :first-child {
        width: 0%;
        animation: ${fadeOut} 0.2s forwards ease-in-out;
      }
      :last-child {
        width: 100%;
        animation: ${fadeIn} 0.2s forwards ease-in-out;
      }
    }
  }
  .date-selection {
    margin-top: 5px !important;
    margin-bottom: 10px !important;
  }
  .date-section > *:first-child {
    margin-bottom: 10px !important;
  }
  .item-input {
    align-items: center;
    .date-view {
      margin: 10px 0;
    }
    input {
      margin: 10px 0;
      padding-top: 5px;
    }
    > *:first-child {
      flex: 2;
    }
    > *:last-child {
      flex: 4;
    }
  }
  .list > *:first-child {
    margin-bottom: 0 !important;
  }

  .h1 {
    font-size: 24px;
    text-transform: uppercase;
  }
  &.mobile .h1 {
    font-size: 20px;
  }

  .rdp,
  .rdp .rdp-months,
  .rdp .rdp-months .rdp-month,
  .rdp .rdp-months .rdp-month .rdp-table {
    width: 100%;
  }
  .rdp {
    margin-top: 15px;
    border: solid 1px;
    .rdp-caption {
      border-bottom: solid 1px;
      padding: 10px 15px 5px 15px;
      position: relative;
    }
    .rdp-nav {
      display: flex;
      justify-content: space-between;
      align-items: center;
      z-index: 3;
      position: relative;
    }
    .rdp-caption_label {
      text-align: center;
      position: absolute;
      width: calc(100% - 30px);
    }
    .rdp-table {
      padding: 10px 0;
    }
    .rdp-cell {
      text-align: center;
      padding: 3px;
    }
    button.rdp-button_reset.rdp-button.rdp-day.rdp-day_disabled {
      opacity: 0.2;
    }
    .rdp-button:hover:not([disabled]):not(.rdp-day_selected) {
      color: #f3d7c8;
    }
  }
`
