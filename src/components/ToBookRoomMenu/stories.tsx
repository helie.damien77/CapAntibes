import { Story, Meta } from '@storybook/react'
import ToBookRoomMenu from '.'

export default {
  title: 'ToBookRoomMenu',
  component: ToBookRoomMenu
} as Meta

export const Default: Story = () => <ToBookRoomMenu />
