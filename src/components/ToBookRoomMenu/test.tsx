import { render, screen } from '@testing-library/react'

import ToBookRoomMenu from '.'

describe('<ToBookRoomMenu />', () => {
  it('should render the heading', () => {
    const { container } = render(<ToBookRoomMenu />)

    expect(
      screen.getByRole('heading', { name: /ToBookRoomMenu/i })
    ).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
