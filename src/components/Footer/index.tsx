import {
  contentsAdmin,
  contentsContact,
  contentsNewsLetter,
  contentsSocialLinks
} from '@/contents/globals'
import { useIsLaptop, useLink, useText } from '@/hooks'
import { getFontSize } from '@/utils'
import React, { useContext, useEffect, useState } from 'react'
import Button from '../Button'
import Divider from '../Divider'
import ExternalLink from '../ExternalLink'
import Input from '../Input'
import Text from '../Text'
import * as S from './styles'
import Image from 'next/image'
import imgCastle from '../../../public/assets/images/global/castle.svg'
import Column from '../Column'
import Row from '../Row'
import { TCssSize } from '@/interfaces'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { LangContext } from '@/stores'
import Languages from '@/enums/languages'

const linkHiring = 'https://careers.werecruit.io/fr/chapitre-six'
const linkHotels = 'https://chapitresixhotels.com/'
const linkPlan = 'solene@pascalevenot.fr'
import imgButton from '../../../public/assets/images/collection/button.png'
import axios from 'axios'

const Section = ({ children }: { children: React.ReactNode }) => (
  <>
    <Divider />
    <S.Section>{children}</S.Section>
  </>
)

type ContentProps = {
  fontSize: string
}
const Contact = ({ fontSize }: ContentProps) => {
  const texts = {
    address: useText(contentsContact.address),
    zipCode: useText(contentsContact.zipCode),
    city: useText(contentsContact.city),
    email: useText(contentsContact.email),
    phone: useText(contentsContact.phone),
    country: useText(contentsContact.country)
  }
  return (
    <Section>
      <Column>
        <S.Part direction="row" align="center" fontSize={fontSize}>
          <Text opt_align="center">{texts.address}</Text>
        </S.Part>
        <S.Part direction="row" align="center" fontSize={fontSize}>
          <Text>
            {texts.zipCode} &nbsp; {texts.city}
          </Text>
        </S.Part>
        <S.Part direction="row" align="space-between" fontSize={fontSize}>
          <ExternalLink link={`mailto:${texts.email}`}>
            <Text>{texts.email}</Text>
          </ExternalLink>
          <ExternalLink link={`tel:${texts.phone}`}>
            <Text>{texts.phone}</Text>
          </ExternalLink>
        </S.Part>
      </Column>
    </Section>
  )
}
const Social = ({ fontSize }: ContentProps) => {
  const router = useRouter()
  const [links, setLinks] = React.useState({
    whatsapp: contentsSocialLinks.whatsapp.link,
    facebook: contentsSocialLinks.facebook.link,
    instagram: contentsSocialLinks.instagram.link,
    linkedin: contentsSocialLinks.linkedin.link
  })

  useEffect(() => {
    if (
      ['/restaurant-les-pecheurs', '/en/les-pecheurs-restaurant'].includes(
        router.asPath
      )
    ) {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.linkRestoPecheur,
        facebook: contentsSocialLinks.facebook.linkRestoPecheur,
        instagram: contentsSocialLinks.instagram.linkRestoPecheur,
        linkedin: contentsSocialLinks.linkedin.linkRestoPecheur
      })
    } else if (
      ['/restaurant-baba', '/en/baba-restaurant'].includes(router.asPath)
    ) {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.linkRestoBaba,
        facebook: contentsSocialLinks.facebook.linkRestoBaba,
        instagram: contentsSocialLinks.instagram.linkRestoBaba,
        linkedin: contentsSocialLinks.linkedin.linkRestoBaba
      })
    } else {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.link,
        facebook: contentsSocialLinks.facebook.link,
        instagram: contentsSocialLinks.instagram.link,
        linkedin: contentsSocialLinks.linkedin.link
      })
    }
  }, [router.asPath])

  return (
    <Section>
      <S.Part
        className="part-m"
        style={{ alignItems: 'flex-start' }}
        direction="column"
        align="space-between"
        fontSize={fontSize}
      >
        <ExternalLink link={links.whatsapp}>
          <Text>{contentsSocialLinks.whatsapp.label}</Text>
        </ExternalLink>
        <ExternalLink link={links.facebook}>
          <Text>{contentsSocialLinks.facebook.label}</Text>
        </ExternalLink>
      </S.Part>
      <S.Part
        className="part-m"
        style={{ alignItems: 'flex-end' }}
        direction="column"
        align="space-between"
        fontSize={fontSize}
      >
        <ExternalLink link={links.instagram}>
          <Text>{contentsSocialLinks.instagram.label}</Text>
        </ExternalLink>
        <ExternalLink link={links.linkedin}>
          <Text>{contentsSocialLinks.linkedin.label}</Text>
        </ExternalLink>
      </S.Part>
    </Section>
  )
}
const Admin = ({ fontSize }: ContentProps) => {
  const texts = {
    plan: useText(contentsAdmin.plan),
    legalNotice: useText(contentsAdmin.legalNotice),
    hiring: useText(contentsAdmin.hiring),
    hotels: useText(contentsAdmin.hotels)
  }
  return (
    <Section>
      <S.Part
        className="part-m"
        style={{ alignItems: 'flex-start' }}
        direction="column"
        align="space-between"
        fontSize={fontSize}
      >
        <Text>
          <Image
            style={{ height: fontSize, marginRight: '5px' }}
            src={imgCastle}
            alt="logo"
          />{' '}
          {texts.hotels}
        </Text>
        <Link shallow href={useLink('/mentions-legales')}>
          <Text>{texts.legalNotice}</Text>
        </Link>
      </S.Part>
      <S.Part
        className="part-m"
        style={{ alignItems: 'flex-end' }}
        direction="column"
        align="space-between"
        fontSize={fontSize}
      >
        <ExternalLink link={`mailto:${linkPlan}`}>
          <Text>{texts.plan}</Text>
        </ExternalLink>
        <ExternalLink link={linkHiring}>
          <Text>{texts.hiring}</Text>
        </ExternalLink>
      </S.Part>
    </Section>
  )
}

type FooterProps = React.ComponentPropsWithoutRef<'footer'>
const Footer = (props: FooterProps) => {
  const { lang } = useContext(LangContext)
  const [links, setLinks] = React.useState({
    whatsapp: contentsSocialLinks.whatsapp.link,
    facebook: contentsSocialLinks.facebook.link,
    instagram: contentsSocialLinks.instagram.link,
    linkedin: contentsSocialLinks.linkedin.link
  })
  const mentionLegalLink = useLink('/mentions-legales')
  const router = useRouter()
  const isLaptop = useIsLaptop()
  const spacing: TCssSize = {
    unit: 'px',
    value: 15
  }

  const [newsletterMail, setNewsletterMail] = useState<string>('')
  const [mailSubmitted, setMailSubmitted] = useState(false)
  const addContactToBrevo = async (listId: number) => {
    if (!newsletterMail) {
      return
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(newsletterMail)
    ) {
      return
    }

    try {
      const response = await axios.post('/api/contacts', {
        mail: newsletterMail,
        listId: listId
      })

      setMailSubmitted(true)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    if (
      ['/restaurant-les-pecheurs', '/en/les-pecheurs-restaurant'].includes(
        router.asPath
      )
    ) {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.linkRestoPecheur,
        facebook: contentsSocialLinks.facebook.linkRestoPecheur,
        instagram: contentsSocialLinks.instagram.linkRestoPecheur,
        linkedin: contentsSocialLinks.linkedin.linkRestoPecheur
      })
    } else if (
      ['/restaurant-baba', '/en/baba-restaurant'].includes(router.asPath)
    ) {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.linkRestoBaba,
        facebook: contentsSocialLinks.facebook.linkRestoBaba,
        instagram: contentsSocialLinks.instagram.linkRestoBaba,
        linkedin: contentsSocialLinks.linkedin.linkRestoBaba
      })
    } else {
      setLinks({
        whatsapp: contentsSocialLinks.whatsapp.link,
        facebook: contentsSocialLinks.facebook.link,
        instagram: contentsSocialLinks.instagram.link,
        linkedin: contentsSocialLinks.linkedin.link
      })
    }
  }, [router.asPath])

  const texts = {
    label: useText(contentsNewsLetter.label),
    labelBis: useText(contentsNewsLetter.labelBis),
    placeHolder: useText(contentsNewsLetter.placeHolder),
    button: useText(contentsNewsLetter.button),
    buttonBis: useText(contentsNewsLetter.buttonBis),
    collection: useText(contentsContact.collection),
    labelAddress: useText(contentsContact.label),
    address: useText(contentsContact.address),
    zipCode: useText(contentsContact.zipCode),
    city: useText(contentsContact.city),
    email: useText(contentsContact.email),
    phone: useText(contentsContact.phone),
    country: useText(contentsContact.country),
    plan: useText(contentsAdmin.plan),
    backToTop: useText(contentsAdmin.backToTop),
    legalNotice: useText(contentsAdmin.legalNotice),
    hiring: useText(contentsAdmin.hiring),
    hotels: useText(contentsAdmin.hotels),
    submitted: useText({
      en: 'Email has been registered',
      fr: 'Votre email a bien été enregistré',
      ru: 'Email has been registered'
    })
  }
  const fontSizeLaptop = {
    regular: {
      current: '1.1vw',
      middle: 17
    },
    small: {
      current: '1.2vw',
      middle: 14
    }
  }
  const fontSizeMobile = {
    regular: {
      current: '2.2vw',
      middle: 17
    },
    small: {
      current: '2.2vw',
      middle: 12
    }
  }

  const fontSize = isLaptop ? fontSizeMobile : fontSizeLaptop
  const fontRegular = getFontSize(
    fontSize.regular.current,
    fontSize.regular.middle
  )
  const fontSmall = getFontSize(fontSize.small.current, fontSize.small.middle)

  if (!isLaptop) {
    return (
      <S.Wrapper className={`mobile sc-footer ${props.className}`} {...props}>
        <Section>
          <S.Part
            direction="column"
            align="space-between"
            fontSize={fontRegular}
          >
            {mailSubmitted && <Text>{texts.submitted}</Text>}
            {!mailSubmitted && (
              <form>
                <Text>{texts.label}</Text>
                <Input
                  type="email"
                  placeholder={texts.placeHolder}
                  opt_fontSize={fontRegular}
                />
                <Button type="submit">
                  <Text>{texts.button}</Text>
                </Button>
              </form>
            )}
          </S.Part>
        </Section>
        <Contact fontSize={fontRegular} />
        <Social fontSize={fontSmall} />
        <Admin fontSize={fontSmall} />
      </S.Wrapper>
    )
  }

  return (
    <S.Wrapper className={`large sc-footer ${props.className}`} {...props}>
      <Section>
        <Column
          style={{ flex: 2 }}
          opt_spacing={spacing}
          opt_alignItems="flex-start"
        >
          <Text>{texts.address}</Text>
          <ExternalLink link={`mailto:${texts.email}`}>
            <Text>{texts.email}</Text>
          </ExternalLink>
          <ExternalLink link={links.whatsapp}>
            <Text>{contentsSocialLinks.whatsapp.label}</Text>
          </ExternalLink>
        </Column>
        <Column
          style={{ flex: 3 }}
          opt_spacing={spacing}
          opt_alignItems="stretch"
        >
          <ExternalLink
            // className="collection"
            className="collection-full"
            link={`${linkHotels}${lang === Languages.EN ? 'en/' : ''}`}
          >
            <Text opt_align="center">{texts.labelAddress}</Text>
            {/* {texts.collection.split(' ').map((item, index) => (
              <Text key={index} className="first">
                {item}
              </Text>
            ))} */}
          </ExternalLink>
          <Row opt_justifyContent="space-between">
            <Link shallow href={mentionLegalLink}>
              <Text>{texts.legalNotice}</Text>
            </Link>
            <ExternalLink link={linkHiring}>
              <Text opt_align="right">{texts.hiring}</Text>
            </ExternalLink>
          </Row>

          {mailSubmitted && <Text opt_align="center">{texts.submitted}</Text>}
          {!mailSubmitted && (
            <form className="large-form">
              <Text opt_align="right">{texts.labelBis}</Text>
              <Input
                className="large"
                type="email"
                opt_fontSize={fontRegular}
                onChange={(e) => setNewsletterMail(e.target.value)}
              />
              <button
                type="submit"
                onClick={() =>
                  addContactToBrevo(
                    lang === Languages.FR ? 65 : lang === Languages.EN ? 67 : 68
                  )
                }
              >
                <Text opt_align="left">{texts.buttonBis}</Text>
              </button>
            </form>
          )}
        </Column>
        <Column
          style={{ flex: 2 }}
          opt_spacing={spacing}
          opt_alignItems="flex-end"
        >
          <Text>
            {texts.zipCode} &nbsp; {texts.city}
          </Text>
          <ExternalLink link={`tel:${texts.phone}`}>
            <Text>{texts.phone}</Text>
          </ExternalLink>
          <ExternalLink link={links.linkedin}>
            <Text>{contentsSocialLinks.linkedin.label}</Text>
          </ExternalLink>
        </Column>
      </Section>
      <S.Section style={{ marginTop: 0 }}>
        <Row opt_justifyContent="space-between" opt_alignItems="center">
          <Text>
            <Image
              style={{ height: fontSmall, marginRight: '10px' }}
              src={imgCastle}
              alt="logo"
            />
            {texts.hotels}
          </Text>
          <ExternalLink link={links.instagram}>
            <Text opt_align="center">
              {contentsSocialLinks.instagram.label}
            </Text>
          </ExternalLink>
          <ExternalLink
            link={
              lang === Languages.FR
                ? 'https://chapitresixhotels.com/'
                : 'https://chapitresixhotels.com/en'
            }
          >
            <Column opt_alignItems="center" opt_justifyContent="center">
              <Image
                className="btn"
                id="address-btn"
                src={imgButton}
                alt="adresses"
              />
            </Column>
          </ExternalLink>
          <ExternalLink link={links.facebook}>
            <Text opt_align="center">{contentsSocialLinks.facebook.label}</Text>
          </ExternalLink>
          <ExternalLink link={`mailto:${linkPlan}`}>
            <Text opt_align="right">{texts.plan}</Text>
          </ExternalLink>
        </Row>
      </S.Section>
    </S.Wrapper>
  )
}

export default Footer
