import { render, screen } from '@testing-library/react'

import RgpdBar from '.'

describe('<RgpdBar />', () => {
  it('should render the heading', () => {
    const { container } = render(<RgpdBar />)

    expect(
      screen.getByRole('heading', { name: /RgpdBar/i })
    ).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
