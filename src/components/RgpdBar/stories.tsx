import { Story, Meta } from '@storybook/react'
import RgpdBar from '.'

export default {
  title: 'RgpdBar',
  component: RgpdBar
} as Meta

export const Default: Story = () => <RgpdBar />
