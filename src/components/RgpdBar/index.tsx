import { useIsLaptop, useText } from '@/hooks'
import { useEffect, useState } from 'react'
import * as S from './styles'
import RowToColumn from '../RowToColumn'
import Text from '../Text'
import { contentsRgpd } from '@/contents/globals/rgpd'
import Button from '../Button'

const RgpdBar = () => {
  const [showBanner, setShowBanner] = useState(false)
  const isLaptop = useIsLaptop()
  const rgpdText = useText(contentsRgpd.text)
  const rgpdBtn = useText(contentsRgpd.button)

  useEffect(() => {
    const rgpdApproved = localStorage.getItem('rgpd_approved')
    if (rgpdApproved === 'true') {
      setShowBanner(false)
    } else {
      setShowBanner(true)
    }
  }, [])

  const handleAccept = () => {
    localStorage.setItem('rgpd_approved', 'true')
    setShowBanner(false)
  }

  if (!showBanner) {
    return null
  }

  return (
    <S.Wrapper className={!isLaptop ? 'mobile' : ''}>
      <RowToColumn
        align="center"
        marginForColumn={[0, 0, 0, 0]}
        marginForRow={[0, 0, 0, 0]}
      >
        <Text className="p">{rgpdText}</Text>
        <Button className="btn" onClick={handleAccept}>
          <Text>{rgpdBtn}</Text>
        </Button>
      </RowToColumn>
    </S.Wrapper>
  )
}

export default RgpdBar
