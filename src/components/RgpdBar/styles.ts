import styled from 'styled-components'

export const Wrapper = styled.div`
  position: fixed;
  bottom: 0;
  &.mobile {
    bottom: ${(props) => props.theme.bottomBar}px;
  }
  z-index: 3;

  height: fit-content;
  width: 100%;

  padding: 10px 0;

  > div {
    justify-content: center;
  }

  .p,
  .p > *,
  .btn,
  .btn > * {
    font-size: 9px;
  }
  .p,
  .btn {
    width: fit-content;
  }
  .p {
    padding: 0 5px;
    text-transform: none;
    text-align: center;
  }
  .btn {
    padding: 3px 0 3px 10px;
    border: 2px solid ${(props) => props.theme.fontColors.primary};
    margin-left: 10px;
  }
  &.mobile .btn {
    margin-top: 10px;
  }
  font-family: ${(props) => props.theme.fontStyles.medium};
  text-transform: uppercase;

  color: ${(props) => props.theme.fontColors.primary};
  background-color: ${(props) => props.theme.bgColors.primary};

  display: flex;
  justify-content: center;
  align-items: center;

  border-top: 2px solid ${(props) => props.theme.fontColors.primary};
`
