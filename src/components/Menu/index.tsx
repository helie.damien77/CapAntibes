import { contentsLayout } from '@/contents/globals'
import Languages from '@/enums/languages'
import { useIsLaptop, useLink, useText } from '@/hooks'
import { MenuContext } from '@/stores'
import { LangContext } from '@/stores'
import Link from 'next/link'
import { useContext, useEffect, useState } from 'react'
import * as S from './styles'
import { urls } from '@/utils'
import { useRouter } from 'next/router'

const Menu = () => {
  const router = useRouter()
  const { lang, setLang } = useContext(LangContext)
  const { menuOpen } = useContext(MenuContext)
  const [menuStatus, setMenuStatus] = useState<'close' | 'open' | 'undefined'>(
    'undefined'
  )
  const [showItems, setShowItems] = useState<
    null | 'hotel' | 'room' | 'restaurant' | 'farniente'
  >(null)
  const isLaptop = useIsLaptop()
  const labelSize = !isLaptop ? '24px' : '56px'
  const labelHeight = !isLaptop ? '40px' : '90px'
  const linkSize = !isLaptop ? '15px' : '26px'
  const linkHeight = !isLaptop ? '35px' : '50px'

  const texts = {
    hotel: {
      main: useText(contentsLayout.hotel),
      history: useText(contentsLayout.hotel_history),
      gallery: useText(contentsLayout.hotel_gallery),
      localisation: useText(contentsLayout.hotel_localisation),
      collection: useText(contentsLayout.hotel_collection)
    },
    rooms: {
      main: useText(contentsLayout.rooms),
      deluxe: useText(contentsLayout.rooms_deluxe),
      privilege: useText(contentsLayout.rooms_privilege),
      seaview: useText(contentsLayout.rooms_seaview),
      design: useText(contentsLayout.rooms_design),
      executive: useText(contentsLayout.rooms_executive),
      connecting: useText(contentsLayout.rooms_connecting)
    },
    restaurant: {
      main: useText(contentsLayout.restaurant),
      restau: useText(contentsLayout.restaurant_restaurants),
      fishing: useText(contentsLayout.restaurant_fishing),
      beach: useText(contentsLayout.restaurant_beach)
    },
    farniente: useText(contentsLayout.farniente),
    wellness: useText(contentsLayout.wellness),
    privatization: useText(contentsLayout.privatization)
  }
  const handleLangClick = (lang: Languages) => {
    const newPath = urls.find((item) =>
      [item[Languages.FR], item[Languages.EN], item[Languages.RU]].includes(
        router.route
      )
    )
    if (newPath) {
      router.push(newPath[lang], undefined, { shallow: true })
    }
    setLang(lang)
  }

  const handleClick = (
    value: 'hotel' | 'room' | 'restaurant' | 'farniente'
  ) => {
    const newValue = showItems === value ? null : value
    setShowItems(newValue)
  }

  useEffect(() => {
    switch (menuOpen) {
      case 'menu':
        setMenuStatus('open')
        break
      case 'menu-close':
        setMenuStatus('close')
        break
      default:
        setMenuStatus('undefined')
        break
    }
  }, [menuOpen])

  return (
    <S.Wrapper
      data-open={menuStatus}
      className={`sc-menu ${!isLaptop ? 'mobile' : ''}`}
    >
      <div className="list">
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            onClick={() => handleClick('hotel')}
            style={{ lineHeight: labelHeight, fontSize: labelSize, zIndex: 3 }}
          >
            {texts.hotel.main}
          </div>
          <S.SubItem
            className={`${showItems === 'hotel' ? 'active' : ''}`}
            style={{ lineHeight: linkHeight, fontSize: linkSize }}
          >
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/histoire-du-cap')}>
                <div className="menu-link">{texts.hotel.history}</div>
              </Link>
              <Link shallow href={useLink('/la-galerie')}>
                <div className="menu-link">{texts.hotel.gallery}</div>
              </Link>
            </S.Link>
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/localisation')}>
                <div className="menu-link">{texts.hotel.localisation}</div>
              </Link>
              <Link shallow href={useLink('/la-collection')}>
                <div className="menu-link">{texts.hotel.collection}</div>
              </Link>
            </S.Link>
          </S.SubItem>
        </S.Item>
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            onClick={() => handleClick('room')}
            style={{ lineHeight: labelHeight, fontSize: labelSize, zIndex: 3 }}
          >
            {texts.rooms.main}
          </div>
          <S.SubItem
            className={`${showItems === 'room' ? 'active' : ''}`}
            style={{ lineHeight: linkHeight, fontSize: linkSize }}
          >
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/chambre-deluxe')}>
                <div className="menu-link">{texts.rooms.deluxe}</div>
              </Link>
              <Link shallow href={useLink('/chambre-privilege')}>
                <div className="menu-link">{texts.rooms.privilege}</div>
              </Link>
            </S.Link>
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/chambre-privilege-vue-mer')}>
                <div className="menu-link">{texts.rooms.seaview}</div>
              </Link>
              <Link shallow href={useLink('/suite')}>
                <div className="menu-link">{texts.rooms.design}</div>
              </Link>
            </S.Link>
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/suite-le-cap')}>
                <div className="menu-link">{texts.rooms.executive}</div>
              </Link>
              <Link shallow href={useLink('/chambres-communicantes')}>
                <div className="menu-link">{texts.rooms.connecting}</div>
              </Link>
            </S.Link>
          </S.SubItem>
        </S.Item>
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            onClick={() => handleClick('restaurant')}
            style={{ lineHeight: labelHeight, fontSize: labelSize, zIndex: 3 }}
          >
            {texts.restaurant.main}
          </div>
          <S.SubItem
            className={`${showItems === 'restaurant' ? 'active' : ''}`}
            style={{ lineHeight: linkHeight, fontSize: linkSize }}
          >
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/restaurants-bar')}>
                <div className="menu-link">{texts.restaurant.restau}</div>
              </Link>
            </S.Link>
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/restaurant-les-pecheurs')}>
                <div className="menu-link">{texts.restaurant.fishing}</div>
              </Link>
            </S.Link>
            <S.Link
              style={{
                flexDirection: isLaptop ? 'row' : 'column'
              }}
            >
              <Link shallow href={useLink('/restaurant-baba')}>
                <div className="menu-link">{texts.restaurant.beach}</div>
              </Link>
            </S.Link>
          </S.SubItem>
        </S.Item>
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            style={{
              lineHeight: labelHeight,
              fontSize: labelSize,
              zIndex: 3
            }}
          >
            <Link className="a-link" shallow href={useLink('/farniente')}>
              {texts.farniente}
            </Link>
          </div>
        </S.Item>
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            style={{
              lineHeight: labelHeight,
              fontSize: labelSize,
              zIndex: 3
            }}
          >
            <Link className="a-link" shallow href={useLink('/bien-etre')}>
              {texts.wellness}
            </Link>
          </div>
        </S.Item>
        <S.Item className={!isLaptop ? 'mobile' : ''}>
          <div
            className="menu-item"
            style={{
              lineHeight: labelHeight,
              fontSize: labelSize,
              zIndex: 3
            }}
          >
            <Link className="a-link" shallow href={useLink('/privatisation')}>
              {texts.privatization}
            </Link>
          </div>
        </S.Item>
      </div>
      {!isLaptop && (
        <div className="lang">
          <div
            className={`${lang === Languages.FR ? 'selected' : ''}`}
            onClick={() => handleLangClick(Languages.FR)}
          >
            {Languages.FR}
          </div>
          <div
            className={`${lang === Languages.EN ? 'selected' : ''}`}
            onClick={() => handleLangClick(Languages.EN)}
          >
            {Languages.EN}
          </div>
          {/* <div
            className={`${lang === Languages.RU ? 'selected' : ''}`}
            onClick={() => handleLangClick(Languages.RU)}
          >
            {Languages.RU}
          </div> */}
        </div>
      )}
    </S.Wrapper>
  )
}

export default Menu
