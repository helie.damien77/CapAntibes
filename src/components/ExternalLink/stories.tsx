import { Story, Meta } from '@storybook/react'
import ExternalLink from '.'

export default {
  title: 'ExternalLink',
  component: ExternalLink
} as Meta

export const Default: Story = () => <ExternalLink link="/" />
