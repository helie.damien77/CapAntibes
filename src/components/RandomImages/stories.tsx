import { Story, Meta } from '@storybook/react'
import RandomImage from '.'

export default {
  title: 'RandomImage',
  component: RandomImage
} as Meta

export const Default: Story = () => <RandomImage listImages={[]} />
