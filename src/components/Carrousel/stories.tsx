import { Story, Meta } from '@storybook/react'
import Carrousel from '.'

export default {
  title: 'Carrousel',
  component: Carrousel
} as Meta

export const Default: Story = () => <Carrousel images={[]} />
