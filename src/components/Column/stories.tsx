import { Story, Meta } from '@storybook/react'
import Column from '.'

export default {
  title: 'Column',
  component: Column
} as Meta

export const Default: Story = () => <Column />
