import styled from 'styled-components'

export const Wrapper = styled.div`
  z-index: 10;
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
`
export const Container = styled.div`
  width: 50vw;
  height: 60vh;
  background-color: white;
  position: relative;

  .btn-close {
    position: absolute;
    right: 10px;
    top: 10px;
    width: 15px;
    height: 15px;
  }

  .content {
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 100%;
  }

  .content-part {
    width: 50%;
    height: 100%;
  }
  .content-img {
    display: block;
  }
  .content-text {
    padding: 5% 5%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 10px;
  }

  &.mobile {
    margin-top: -20%;
    width: 84vw;
    height: 44vh;
    .content-part {
      width: 100%;
      height: 100%;
    }
  }

  .h1,
  .h3,
  p,
  .btn {
    line-height: 18px;
    text-align: center;
  }
  .h1 {
    font-size: 16px;
    text-transform: uppercase;
  }
  .h3 {
    font-size: 14px;
    text-transform: uppercase;
  }
  .btn {
    margin-top: 15px;
    text-transform: uppercase;
    font-size: 10px;
    p,
    svg {
      height: 18px;
    }
  }
  .p {
    font-size: 10px;
    p {
      text-align: center;
      text-transform: none;
    }
  }
`
