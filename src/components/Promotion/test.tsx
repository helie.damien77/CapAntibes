import { render, screen } from '@testing-library/react'

import Promotion from '.'
import React from 'react'

describe('<Promotion />', () => {
  it('should render the heading', () => {
    const { container } = render(<Promotion handleCloseClick={() => {}} />)

    expect(
      screen.getByRole('heading', { name: /Promotion/i })
    ).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
