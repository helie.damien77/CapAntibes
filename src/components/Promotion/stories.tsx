import { Story, Meta } from '@storybook/react'
import Promotion from '.'
import React from 'react'

export default {
  title: 'Promotion',
  component: Promotion
} as Meta

export const Default: Story = () => <Promotion handleCloseClick={() => {}} />
