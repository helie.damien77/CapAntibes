import { useContext } from 'react'
import * as S from './styles'
import { LangContext } from '@/stores'
import { useIsLaptop, useLink, useText } from '@/hooks'
import { contentsHome } from '@/contents/pages'
import Link from 'next/link'
import Button from '../Button'
import Column from '../Column'
import Row from '../Row'
import promotion from '../../../public/assets/images/home/popup-retraite.jpg'
import Image from 'next/image'
import Text from '../Text'
import ExternalLink from '../ExternalLink'
import Languages from '@/enums/languages'

type PromotionProps = {
  handleCloseClick: () => void
}
const Promotion = ({ handleCloseClick }: PromotionProps) => {
  const { lang } = useContext(LangContext)
  const isLaptop = useIsLaptop()

  const titleByLang = {
    fr: `La retraite «Le Printemps du yoga»`,
    en: `Springtime Yoga Retreat`,
    ru: `TWO PLACES, ONE ATMOSPHERE, SIX HANDS`
  }

  const textByLang = {
    fr: `Du 4 au 7 avril, offrez-vous un temps pour ralentir, vous ressourcer et savourer la douceur du printemps méditerranéen... Une parenthèse de mer et de sable, de délices et de parfums du sud. Au programme, quatre jours de yin & yang yoga, méditations en mouvement, chant, danse intuitive et musique live.`,
    en: `From April 4 to 7, treat yourself to a time to slow down, recharge your batteries and savor the sweetness of Mediterranean spring... An interlude of sea and sand, southern delights and fragrances. On the program: four days of yin & yang yoga, meditation in movement, chanting, intuitive dance and live music.`,
    ru: `The Cap d'Antibes Beach Hotel celebrates July 14th with an exclusive menu. To celebrate the French national holiday, Les Pêcheurs Restaurante con estrella Michelin and Baba restaurants combine their expertise to create an exceptional culinary experience. Set to live music, the two restaurants are transformed for a moment of conviviality and sharing.`
  }

  const actionByLang = {
    fr: 'En savoir plus',
    en: 'Read more',
    ru: 'menu'
  }
  
  const action2ByLang = {
    fr: 'Réserver',
    en: 'Book',
    ru: 'Book'
  }

  const title = titleByLang[`${lang}`]
  const text = textByLang[`${lang}`]
  const action = actionByLang[`${lang}`]
  const action2 = action2ByLang[`${lang}`]

  return (
    <S.Wrapper className={`sc-promotion `} onClick={handleCloseClick}>
      <S.Container className={!isLaptop ? 'mobile' : ''}>
        <svg
          className="btn-close"
          onClick={handleCloseClick}
          width="29"
          height="29"
          viewBox="0 0 29 29"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M1 1L27.5 27.5" stroke="#D99B81" strokeWidth="2" />
          <path d="M27.5 1L0.999999 27.5" stroke="#D99B81" strokeWidth="2" />
        </svg>
        <div className="content">
          {isLaptop && (
            <div className="content-part content-img">
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover'
                }}
                src={promotion}
                alt="Illustration pour la promotion"
              />
            </div>
          )}
          <div className="content-part content-text">
            <Text className="h1">{title}</Text>
            {/* <Text className="h3">{title}</Text> */}
            <Text className="p">{text}</Text>
            {/* <Link shallow href={useLink('/farniente')}> */}
            {<Link
              shallow
              href={`https://capdantibes-beachhotel.com/pdfs/retraite-yoga.pdf`}
            >
              <Button className="btn">
                <Text>{action}</Text>
              </Button>
            </Link>}

            <Link
              shallow
              href={`mailto:reception@cabh.fr`}
            >
              <Button className="btn">
                <Text>{action2}</Text>
              </Button>
            </Link>

            {/* <ExternalLink link={`mailto:contact@mail.com`}>
              <Button className="btn">
                <Text>{action}</Text>
              </Button>
            </ExternalLink> */}
          </div>
        </div>
      </S.Container>
    </S.Wrapper>
  )
}

export default Promotion
