import { Story, Meta } from '@storybook/react'
import BottomBar from '.'

export default {
  title: 'BottomBar',
  component: BottomBar
} as Meta

export const Default: Story = () => <BottomBar />
