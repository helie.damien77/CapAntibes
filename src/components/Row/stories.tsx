import { Story, Meta } from '@storybook/react'
import Row from '.'

export default {
  title: 'Row',
  component: Row
} as Meta

export const Default: Story = () => <Row />
