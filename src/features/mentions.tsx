import Head from 'next/head'
import Text from '@/components/Text'
import { IMetaPage } from '@/interfaces'
import { MentionsPageStyles } from '@/styles/pages/mentions'
import { useIsLaptop, useLink, useText } from '@/hooks'
import Column from '@/components/Column'
import { contentsMention } from '@/contents/pages/mention'
import Link from 'next/link'

export default function Mentions(meta: IMetaPage) {
  const isLaptop = useIsLaptop()
  return (
    <>
      <MentionsPageStyles />
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main id="mentions" className={!isLaptop ? 'mobile' : ''}>
        <Column opt_margin={isLaptop ? [1, 0, 0, 0] : [3, 1.5, 1.5, 1.5]}>
          <Text className="h1">{useText(contentsMention.title)}</Text>
          <Text className="p">{useText(contentsMention.ligne1)}</Text>
          <Text className="p">{useText(contentsMention.ligne2)}</Text>
          <Text className="p">{useText(contentsMention.ligne3)}</Text>
          <Text className="p">{useText(contentsMention.ligne4)}</Text>
          <Link className="p" href={useLink('www.capdantibes-beachhotel.com')}>
            <Text>{useText(contentsMention.ligne5)}</Text>
          </Link>
          <Text className="p2">{useText(contentsMention.ligne6)}</Text>
          <Text className="p">{useText(contentsMention.ligne7)}</Text>
          <Text className="p">{useText(contentsMention.ligne8)}</Text>
          <Text className="p">{useText(contentsMention.ligne9)}</Text>
          <Text className="p">{useText(contentsMention.ligne10)}</Text>
          <Text className="p">{useText(contentsMention.ligne11)}</Text>
          <Text className="p">{useText(contentsMention.ligne12)}</Text>
          <Text className="p">{useText(contentsMention.ligne13)}</Text>
          <Text className="h1">{useText(contentsMention.title2)}</Text>
          <Text className="h2">{useText(contentsMention.subtitle2)}</Text>
          <Text  className="h1">{useText(contentsMention.title3)}</Text>
          <div className='backlink'>
            <Text className="h2">{useText(contentsMention.subtitle3)}</Text>
            <Link href="https://uhdp.paris/" className="h2">{useText(contentsMention.subtitle3bis)}</Link>
            <Text className="h2">{useText(contentsMention.subtitle3ter)}</Text>
          </div>
          <Text className="h1">{useText(contentsMention.title4)}</Text>
          <Text className="h2">{useText(contentsMention.subtitle4)}</Text>

          <Text className="h1">{useText(contentsMention.title5)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte1)}
          </Text>

          <Text className="h1">{useText(contentsMention.title6)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte2)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte3)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte4)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte5)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte6)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte7)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte8)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte9)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte10)}
          </Text>

          <Text className="h1">{useText(contentsMention.title7)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte11)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte12)}
          </Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte13)}
          </Text>

          <Text className="h1">{useText(contentsMention.title8)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte14)}
          </Text>

          <Text className="h1">{useText(contentsMention.title9)}</Text>
          <Text className="t">{useText(contentsMention.texte15)}</Text>

          <Text className="h1">{useText(contentsMention.title10)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte16)}
          </Text>
          <Text className="p">{useText(contentsMention.ligne14)}</Text>
          <Text className="p">{useText(contentsMention.ligne15)}</Text>
          <Text className="p">{useText(contentsMention.ligne16)}</Text>
          <Text className="p">{useText(contentsMention.ligne17)}</Text>
          <Text className="p">{useText(contentsMention.ligne18)}</Text>
          <Text className="p">{useText(contentsMention.ligne19)}</Text>
          <Text className="p">{useText(contentsMention.ligne20)}</Text>

          <Text className="h1">{useText(contentsMention.title11)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte17)}
          </Text>

          <Text className="h1">{useText(contentsMention.title12)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte18)}
          </Text>

          <Text className="h1">{useText(contentsMention.title13)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte19)}
          </Text>

          <Text className="h1">{useText(contentsMention.title14)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte20)}
          </Text>

          <Text className="h1">{useText(contentsMention.title15)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte21)}
          </Text>

          <Text className="h1">{useText(contentsMention.title16)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte22)}
          </Text>

          <Text className="h1">{useText(contentsMention.title17)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte23)}
          </Text>

          <Text className="h1">{useText(contentsMention.title18)}</Text>
          <Text className="t" opt_align="center">
            {useText(contentsMention.texte24)}
          </Text>
        </Column>
      </main>
    </>
  )
}
