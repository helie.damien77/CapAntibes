import Button from '@/components/Button'
import RowToColumn from '@/components/RowToColumn'
import Text from '@/components/Text'
import { useIsLaptop, useText } from '@/hooks'
import Head from 'next/head'
import img1 from '../../public/assets/images/well-health/well-health_1.jpg'
import img1Mobile from '../../public/assets/images/well-health/well-health_1_mobile.jpg'
import img2 from '../../public/assets/images/well-health/well-health_2.jpg'
import img2Mobile from '../../public/assets/images/well-health/well-health_2_mobile.jpg'
import img3 from '../../public/assets/images/well-health/well-health_3.jpg'
import img3Mobile from '../../public/assets/images/well-health/well-health_3_mobile.jpg'
import img4 from '../../public/assets/images/well-health/well-health_4.jpg'
import img4Mobile from '../../public/assets/images/well-health/well-health_4_mobile.jpg'
import img5 from '../../public/assets/images/well-health/well-health_5.jpg'
import img5Mobile from '../../public/assets/images/well-health/well-health_5_mobile.jpg'

import Image from 'next/image'
import Column from '@/components/Column'
import { IMetaPage, TCssSize } from '@/interfaces'
import {
  contentsPecheursRestaurant,
  contentsWellHealth
} from '@/contents/pages'
import { WellHealthPageStyles } from '@/styles/pages/well-health'
import Link from 'next/link'
import Languages from '@/enums/languages'
import { useContext } from 'react'
import { LangContext } from '@/stores'

export default function WellHealth(meta: IMetaPage) {
  const isLaptop = useIsLaptop()
  const { lang } = useContext(LangContext)
  const valueSpacing = isLaptop ? 20 : 10
  const spacing: TCssSize = { value: valueSpacing, unit: 'px' }
  return (
    <>
      <WellHealthPageStyles />
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main id="well-health" className={!isLaptop ? 'mobile' : ''}>
        <Column
          opt_margin={isLaptop ? [1, 1, 1.5, 1] : [3, 1.5, 1.5, 1.5]}
          style={{ width: `${isLaptop ? '80%' : '100%'}` }}
        >
          <Text className="h1">{useText(contentsWellHealth.title)}</Text>
          <Text className="p">{useText(contentsWellHealth.description)}</Text>
          {/* <Button className="btn">
            <Text>{useText(contentsWellHealth.button)}</Text>
          </Button> */}
        </Column>
        <Image
          style={{ marginBottom: '5%' }}
          className="fullWidth"
          src={isLaptop ? img1 : img1Mobile}
          alt="paysage"
        />
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.5, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
        >
          <Column
            opt_spacing={spacing}
            opt_margin={isLaptop ? [1, 0, 2, 0] : [1, 1.5, 1, 1.5]}
          >
            <Text className="h3">{useText(contentsWellHealth.subTitle1)}</Text>
            <Text className="p">{useText(contentsWellHealth.text1)}</Text>
            <Link
              shallow
              href={
                lang === Languages.FR
                  ? '/pdfs/carte-des-soins-fr.pdf'
                  : '/pdfs/carte-des-soins-en.pdf'
              }
            >
              <Button className="btn">
                <Text>{useText(contentsPecheursRestaurant.menu)}</Text>
              </Button>
            </Link>
          </Column>
          <Column opt_alignItems="flex-end" opt_justifyContent="center">
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img2 : img2Mobile}
              alt="plage"
            />
          </Column>
        </RowToColumn>
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.5, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
          isReverseForColumn={true}
        >
          <Column opt_alignItems="flex-start" opt_justifyContent="center">
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img3 : img3Mobile}
              alt="plage"
            />
          </Column>
          <Column
            opt_spacing={spacing}
            opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}
          >
            <Text className="h3">{useText(contentsWellHealth.subTitle2)}</Text>
            <Text className="p">{useText(contentsWellHealth.text2)}</Text>
            {/* <Button className="btn">
              <Text>{useText(contentsWellHealth.button)}</Text>
            </Button> */}
          </Column>
        </RowToColumn>
        {isLaptop && (
          <Image
            style={{ marginBottom: '5%' }}
            className="fullWidth"
            src={img4}
            alt="paysage"
          />
        )}
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.5, 1.5]}
          marginForColumn={[0, 0, 0, 0]}
        >
          <Column
            opt_spacing={spacing}
            opt_margin={isLaptop ? [1, 0, 2, 0] : [1, 1.5, 1, 1.5]}
          >
            <Text className="h3">{useText(contentsWellHealth.subTitle3)}</Text>
            <Text className="p">{useText(contentsWellHealth.text3)}</Text>
            {/* <Button className="btn">
              <Text>{useText(contentsWellHealth.button)}</Text>
            </Button> */}
          </Column>
          <Column opt_alignItems="flex-end" opt_justifyContent="center">
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img5 : img5Mobile}
              alt="plage"
            />
          </Column>
        </RowToColumn>
        {!isLaptop && (
          <Image
            style={{ marginBottom: '5%', marginTop: '-1.8vw' }}
            className="fullWidth"
            src={img4Mobile}
            alt="paysage"
          />
        )}
      </main>
    </>
  )
}
