import Button from '@/components/Button'
import Row from '@/components/Row'
import RowToColumn from '@/components/RowToColumn'
import Text from '@/components/Text'
import { useIsLaptop, useLink, useText } from '@/hooks'
import Head from 'next/head'
import slider0 from '../../public/assets/images/farniente/slider_0.jpg'
import slider1 from '../../public/assets/images/farniente/slider_1.jpg'
import img1 from '../../public/assets/images/farniente/farniente_1.jpg'
import img1Mobile from '../../public/assets/images/farniente/farniente_1_mobile.jpg'
import img2 from '../../public/assets/images/farniente/farniente_2.jpg'
import img2Mobile from '../../public/assets/images/farniente/farniente_2_mobile.jpg'
import img4 from '../../public/assets/images/farniente/farniente_8.jpg'
import img4Mobile from '../../public/assets/images/farniente/farniente_8_mobile.jpg'
import img5 from '../../public/assets/images/farniente/farniente_4.jpg'
import img5Mobile from '../../public/assets/images/farniente/farniente_4_mobile.jpg'
import img6 from '../../public/assets/images/farniente/farniente_6.jpg'
import img6Mobile from '../../public/assets/images/farniente/farniente_6_mobile.jpg'
import img7 from '../../public/assets/images/farniente/farniente_7.jpg'
import img7Mobile from '../../public/assets/images/farniente/farniente_7_mobile.jpg'
import img8 from '../../public/assets/images/farniente/farniente_5.jpg'
import img8Mobile from '../../public/assets/images/farniente/farniente_5_mobile.jpg'
import random1 from '../../public/assets/images/farniente/random_1.jpg'
import random2 from '../../public/assets/images/farniente/random_2.jpg'
import random1Mobile from '../../public/assets/images/farniente/random_1_mobile.jpg'
import random2Mobile from '../../public/assets/images/farniente/random_2_mobile.jpg'
import Image from 'next/image'
import Column from '@/components/Column'
import { FarnientePageStyles } from '@/styles/pages/farniente'
import { contentsFarniente } from '@/contents/pages/farniente'
import RandomImages from '@/components/RandomImages'
import Carrousel from '@/components/Carrousel'
import InfinitySlider from '@/components/InfinitySlider'
import Link from 'next/link'
import { IMetaPage } from '@/interfaces'

const randomsImages = [random1, random2, random1, random2]
const randomsImagesMobile = [
  random1Mobile,
  random2Mobile,
  random1Mobile,
  random2Mobile
]
export default function Farniente(meta: IMetaPage) {
  const isLaptop = useIsLaptop()
  const getInfinitySliderImageSize = () => {
    const ratio = (slider0.height / slider0.width) * 100
    let width = 100
    let height = ratio
    if (!isLaptop) {
      width = 200
      height = 2 * ratio
    }
    return {
      width,
      height
    }
  }
  return (
    <>
      <FarnientePageStyles />
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main id="farniente" className={!isLaptop ? 'mobile' : ''}>
        {isLaptop ? (
          // <RandomImages listImages={randomsImages} />
          <Row opt_margin={[0, 0, 0, 0]}>
            <Image
              style={{ marginBottom: '5%' }}
              className="fullWidth"
              src={random1}
              alt="paysage"
            />
            <Image
              style={{ marginBottom: '5%' }}
              className="fullWidth"
              src={random2}
              alt="paysage"
            />
          </Row>
        ) : (
          <Carrousel images={randomsImagesMobile} />
        )}
        <Column
          opt_margin={isLaptop ? [1, 1, 1, 1] : [3, 1.5, 1.5, 1.5]}
          style={{ width: `${isLaptop ? '70%' : '100%'}` }}
        >
          <Text className="h1">{useText(contentsFarniente.title)}</Text>
          <Text className="p">{useText(contentsFarniente.description)}</Text>
        </Column>
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0, 1.5, 0.3, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
        >
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle1)}</Text>
            <Text className="p">{useText(contentsFarniente.text1)}</Text>
          </Column>
          <Column
            opt_margin={isLaptop ? [0.2, 0, 0.2, 1] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-end"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img1 : img1Mobile}
              alt="plage"
            />
          </Column>
        </RowToColumn>
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.3, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
          isReverseForColumn={true}
        >
          <Column
            opt_margin={isLaptop ? [0.2, 1, 0.2, 0] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-start"
            opt_justifyContent="center"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img2 : img2Mobile}
              alt="plage"
            />
          </Column>
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle2)}</Text>
            <Text className="p">{useText(contentsFarniente.text2)}</Text>
          </Column>
        </RowToColumn>
        {isLaptop && (
          <Row opt_margin={[1, 0, 1, 0]}>
            <InfinitySlider>
              {[slider0, slider1].map((item) => (
                <Image
                  key={item.src}
                  src={item}
                  alt="liste d'image"
                  style={{
                    cursor: 'initial',
                    width: `${getInfinitySliderImageSize().width}vw`,
                    height: `${getInfinitySliderImageSize().height}vw`
                  }}
                />
              ))}
            </InfinitySlider>
          </Row>
        )}
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.3, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
        >
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle3)}</Text>
            <Text className="p">{useText(contentsFarniente.text3)}</Text>
          </Column>
          <Column
            opt_margin={isLaptop ? [0.2, 0, 0.2, 1] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-end"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img4 : img4Mobile}
              alt="plage"
            />
          </Column>
        </RowToColumn>
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.3, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
          isReverseForColumn={true}
        >
          <Column
            opt_margin={isLaptop ? [0.2, 1, 0.2, 0] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-start"
            opt_justifyContent="center"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto'
              }}
              src={isLaptop ? img5 : img5Mobile}
              alt="plage"
            />
          </Column>
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle4)}</Text>
            <Text className="p">{useText(contentsFarniente.text4)}</Text>
            {/*<Link shallow href="/pdfs/location-pedalos.pdf">
              <Button className="btn">
                <Text>{useText(contentsFarniente.btn4)}</Text>
              </Button>
            </Link>*/}
          </Column>
        </RowToColumn>
        {isLaptop && (
          <Image
            style={{ marginBottom: '5%', marginTop: '5%' }}
            className="fullWidth"
            src={img6}
            alt="paysage"
          />
        )}
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.5, 1.5, 0.3, 1.5]}
          marginForColumn={[0, 0, 1, 0]}
        >
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 1, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle5)}</Text>
            <Text className="p">{useText(contentsFarniente.text5)}</Text>
          </Column>
          <Column
            opt_margin={isLaptop ? [0.2, 0, 0.2, 1] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-end"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto',
                marginBottom: '0vw'
              }}
              src={isLaptop ? img7 : img7Mobile}
              alt="plage"
            />
          </Column>
        </RowToColumn>
        <RowToColumn
          className="section"
          align={isLaptop ? 'center' : 'flex-start'}
          marginForRow={[0.3, 1.5, 1.5, 1.5]}
          marginForColumn={[0, 0, 0, 0]}
          isReverseForColumn={true}
        >
          <Column
            opt_margin={isLaptop ? [0.2, 1, 0.2, 0] : [0.2, 0, 0.2, 0]}
            opt_alignItems="flex-start"
            opt_justifyContent="center"
          >
            <Image
              style={{
                width: `${isLaptop ? '70%' : '100%'}`,
                height: 'auto',
                marginBottom: '0vw'
              }}
              src={isLaptop ? img8 : img8Mobile}
              alt="plage"
            />
          </Column>
          <Column opt_margin={isLaptop ? [0.5, 0, 1, 0] : [1, 1.5, 0, 1.5]}>
            <Text className="h3">{useText(contentsFarniente.subTitle6)}</Text>
            <Text className="p">{useText(contentsFarniente.text6)}</Text>
            {/*<Link shallow href="/pdfs/location-velos.pdf">
              <Button className="btn">
                <Text>{useText(contentsFarniente.btn6)}</Text>
              </Button>
            </Link>*/}
          </Column>
        </RowToColumn>
        {!isLaptop && (
          <Image
            style={{ marginBottom: '5%', marginTop: '-3.6vw' }}
            className="fullWidth"
            src={img6Mobile}
            alt="paysage"
          />
        )}
      </main>
    </>
  )
}
