import Text from '@/components/Text'
import { useIsLaptop, useText } from '@/hooks'
import Head from 'next/head'
import imgButton from '../../public/assets/images/collection/button.png'

import Image from 'next/image'
import Column from '@/components/Column'
import Button from '@/components/Button'
import { CollectionPageStyles } from '@/styles/pages/collection'
import Row from '@/components/Row'
import { contentsCollection } from '@/contents/pages/collection'
import { IMetaPage } from '@/interfaces'
import { useContext } from 'react'
import Languages from '@/enums/languages'
import { LangContext } from '@/stores'

import random0 from '../../public/assets/images/collection/random0.jpg'
import random1 from '../../public/assets/images/collection/random1.jpg'
import random2 from '../../public/assets/images/collection/random2.jpg'
import random3 from '../../public/assets/images/collection/random3.jpg'
import random4 from '../../public/assets/images/collection/random4.jpg'
import random5 from '../../public/assets/images/collection/random5.jpg'
import random6 from '../../public/assets/images/collection/random6.jpg'
import random7 from '../../public/assets/images/collection/random7.jpg'
import RandomImages from '@/components/RandomImages'
import ExternalLink from '@/components/ExternalLink'
import Carrousel from '@/components/Carrousel'

const randomsImages = [
  random0,
  random1,
  random2,
  random3,
  random4,
  random5,
  random6,
  random7
]
export default function Collection(meta: IMetaPage) {
  const isLaptop = useIsLaptop()
  const { lang } = useContext(LangContext)
  return (
    <>
      <CollectionPageStyles />
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main id="collection" className={!isLaptop ? 'mobile' : ''}>
        <Row opt_margin={isLaptop ? [2, 1, 0, 1] : [6, 1.5, 1, 1.5]}>
          <Text className="p">{useText(contentsCollection.introduction)}</Text>
        </Row>
        <Row opt_margin={[0, 1, 0.5, 1]} opt_alignItems="stretch">
          <ExternalLink
            link={
              lang === Languages.FR
                ? 'https://chapitresixhotels.com/'
                : 'https://chapitresixhotels.com/en/'
            }
          >
            <Column opt_alignItems="flex-start" opt_justifyContent="center">
              <Button className="btn">
                <Text>{useText(contentsCollection.button)}</Text>
              </Button>
            </Column>
          </ExternalLink>
          <ExternalLink
            link={
              lang === Languages.FR
                ? 'https://chapitresixhotels.com/'
                : 'https://chapitresixhotels.com/en/'
            }
          >
            <Column opt_alignItems="flex-end" opt_justifyContent="center">
              <Image
                className="btn"
                id="address-btn"
                src={imgButton}
                alt="adresses"
              />
            </Column>
          </ExternalLink>
        </Row>
        {isLaptop ? (
          <RandomImages listImages={randomsImages} />
        ) : (
          <Carrousel images={randomsImages} />
        )}
      </main>
    </>
  )
}
